# Mr. Bacon
This is a simple budget app. It connects to your bank and credit card accounts so you don't need to keep track of receipts.

## Supported Banks
* Wells Fargo (kind of... see below)
* USAA (Credit card)

I have access to way more banks, but no means to test if they work.

## How it works

### Logging into Mr. Bacon
I'm sick of remembering passwords, so I made a log in with Google button. I store the core data that goes along with that.

### Linking accounts
Bank accounts are linked one of two ways:
 1. The easy and not-always-free way:
   * Using [OFX](https://github.com/euforic/banking.js/) which is what Quickbooks and Quicken use. Sometimes banks charge you a monthly fee for access to this. Luckily USAA doesn't charge, but Wells Fargo does. Which leads to the second way.

 2. The hard and probably not legit way:  
   * Logging in and scrapping the data programatically. This means I use you credentials to log in to your bank's website, navigate to the transactions page, and parse the text on that page. I'm sure this breaks the terms of use for every online banking site, but Wells Fargo wanted to charge $10/month for OFX data. I'm not going to be paying money just to get my own transaction data. This way wont make it into a publicly hosted version.  

### Security
I encrypt your credentials using 256bit [AES-GSM](https://en.wikipedia.org/wiki/Galois/Counter_Mode). I will not lie, I know very little about how it works. Your credentials get encrypted, then I send you the encrypted credentials as a file and I store the key on my server. This way, an attacker would need to get into your device and into my server to decrypt your credentials. I may change this up in the future to add another level of security. I could possibly store part of the encrypted credentials somewhere else, but for now, this seems like enough. Before hosting this for others, I'll consult with a security pro.

### Transactions
The UI is inspired by an email client. Transactions come in like emails and your budget categories, like groeries and bills, look like folders. As transaction come in, they appear in the Inbox.
Transactions can be:
 * Moved: to other folders manually or automatically using Rules
 * Split: in case you pull from two categories with a single purchase
 * Hidden: in case you bought a gift, or something embarrassing, and want to hide it from a spouse

### Rules
Rules work the same way as they would in email. You define which folder a transaction goes into if the transaction name starts with, contains, and/or ends with any text. In adition to that, you can sort based on if the transaction amount is exacly, at least, and at most a certain value. There is also a checkbox to define if the rule is a paychek or not. When account syncing happens and a transaction meet a rule, instead of putting the transactions into the Inbox, they get put into the defined folder. If a transaction happens to match more than one rule, the last rule to match is what takes effect.

### Budget
The most important job of Mr. Bacon is budget managment. The goal here is to provide a flexible budget system that accomodates most people's pay cycles (Monthly, semi-monthly, bi-weekly, and weekly). There are two client facing components and one back-end component to making budgets work:
 1. Budget Profiles: the bread and butter of budgets. This is where you define how a paycheck gets split ($ or %) into each folder. It's also where you define how to handle deficit and surplus in each folder. Splitting a paycheck by $ is a little tricky because the amount of money you budget for may be more or less than your actual paycheck, so you can click `remaining` next to a folder to signify that you want the rest of the budget to go into that folder. If you have overbudgeted, then you will be presented with a dialog, when your paycheck arrives, that will prompt you to rebudget given the actual $ you where paid. If, for some reason, handling your paycheck automatically doesn't quite cover your case, you have the option of doing it manually uppon being paid. You can divide you paycheck manually and/or your deficit and surplus manually. You will be presented with the same overbudgetting dialog, but it will only have the sections visible that you marked as manual.
 2. Paycheck Profiles: this is where the paycheck rule is defined and where you determine which budget profile to use based on the date. Let's say you get paid bi-weekly. There are some months where you are paid 3 times a month. I like to call that extra paycheck the bonus paycheck. Now, let's say you have bills due on the 1st and 16th of the month. The way I determine which budget profile to use is by taking the paycheck frequency (bi-weekly in this case) and the defined date that you need that paycheck (1st and 16th) and calculating whether today + 13 days (comes from bi-weekly frequency - 1 because it's including today) contains the defined date. If there are no defined dates within the range, that means you have you bonus paycheck. Based on that calculation a budget profile is used and your current budget ends, causing the deficit and surplus to calculate and resolve based on the last budget profile. Then the new budget profile is used to split the current paycheck.
 3. Budget Id: this is on each transaction and defines which budget a transaction is a part of. The goal here is to enable the ability to move transactions from the old budget to the new one in case they posted to your account at a weird time. It's also how folders calculate their amounts.

## Progress and future plans
[Here's my Trello board](https://trello.com/b/JzlAhM27/mr-bacon)

## Questions, concerns, comments, tips?
Leave an issue, I'll get to it when I can.

## Donate?
[Here's my Square Cash](https://cash.me/$DrewWallace)

## Package info (ignore this for now)
python-shell 0.1.0
banking.js modify getOfxHeaders in ofx.js: /n to /r/n
