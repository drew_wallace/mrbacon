function encrypt(text, givenKey) {
    var crypto = require('crypto'),
        algorithm = 'aes-256-gcm',
        key = (givenKey ? new Buffer(givenKey, 'binary') : crypto.randomBytes(32)),
        iv = crypto.randomBytes(12),
        cipher = crypto.createCipheriv(algorithm, key, iv),
        encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    var tag = cipher.getAuthTag().toString('binary'),
        content = encrypted + tag + iv.toString('binary');
    if(content.indexOf('\n') != -1 || content.indexOf('\r') != -1) {
        return encrypt(text, givenKey);
    } else {
        return {
            content: content,
            key: key.toString('binary')
        };
    }
}

function decrypt(content, key) {
    var crypto = require('crypto'),
        algorithm = 'aes-256-gcm';
    try {
        key = new Buffer(key, 'binary');
        var iv = new Buffer(content.substr(content.length - 12), 'binary')
        var tag = new Buffer(content.substring(content.length - 28, content.length - 12), 'binary');
        var decipher = crypto.createDecipheriv(algorithm, key, iv);
        decipher.setAuthTag(tag);
        var dec = decipher.update(content, 'hex', 'utf8')
        dec += decipher.final('utf8');
    } catch(e) {
        console.log('AES Error:', e);
        return null;
    }
    return dec;
}

module.exports = {encrypt, decrypt};