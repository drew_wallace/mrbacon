'use strict';

const Promise = require('bluebird');
const fs = require('fs');
const Banking = require('banking');
const moment = require('moment');
const _ = require('lodash');
const entities = require('entities');
const numeral = require('numeral');
const AES = require('./aes.js');
const mongo = require('./mongo');
const supportedBanks = require('./supportedBanks');
const puppeteer = require('puppeteer');
require('moment-range');

const User = mongo.User;
const Account = mongo.Account;
const Transaction = mongo.Transaction;
const Folder = mongo.Folder;
const Rule = mongo.Rule;
const BudgetProfile = mongo.BudgetProfile;
const Budget = mongo.Budget;
const Share = mongo.Share;

let banks = {};

exports.ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
};

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route   will be called=> ,
//   which, in this example, will redirect the user to the home page.
exports.googleCallback = (req, res) => {
    User.findOne({id: req.user.id}).exec().then(user => {
        if(!user) {
            const googleUser = new User({
                id: req.user.id,
                name: req.user._json.displayName,
                email: _.get(_.find(req.user._json.emails, email => email.type == 'account'), 'value'),
                image: req.user._json.image.url
            });
            const inbox = new Folder({
                userId: req.user.id,
                name: 'Inbox',
                amount: 0,
                order: 0,
                icon: 'inbox',
                permissions: {
                    edit: false,
                    move: true,
                    budget: false
                }
            });
            const splits = new Folder({
                userId: req.user.id,
                name: 'Splits',
                amount: 0,
                order: 1,
                icon: 'call_split',
                permissions: {
                    edit: false,
                    move: false,
                    budget: false
                }
            });
            const savings = new Folder({
                userId: req.user.id,
                name: 'Savings',
                amount: 0,
                order: 2,
                icon: 'attach_money',
                permissions: {
                    edit: false,
                    move: true,
                    budget: true
                }
            });
            // const firstBudgetProfile = new BudgetProfile({
            //     userId: req.user.id,
            //     name: "Initial Budget Profile",
            //     divisions: {manual: {isManual: true}},
            // });
            // const firstBudget = new Budget({
            //     userId: req.user.id,
            //     date: 20170101000000,
            //     budgetProfileName: ''
            // });
            inbox.save();
            splits.save();
            savings.save();
            // firstBudgetProfile.save();
            // firstBudget.save();
            googleUser.save().then(() => {
                res.redirect('/linkAccounts')
            }).catch(e => {
                console.error(e);
            });
        } else {
            res.redirect('/account');
        }
    }).catch(e => {
        console.error(e);
    });
};

exports.supportedBanksEndpoint = (req, res) => {
    res.json(supportedBanks);
}

exports.logout = (req, res) => {
    req.logOut();
    req.session.destroy(function (err) {
        req.logout();
        res.redirect('/');
    });
};

const account = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        let budgets = await Budget.find({userId}).sort({date: -1}).lean().exec();
        let currentBudgetId = (!req.query.setSelectedBudgetId && req.query.selectedBudgetId) || _.get(budgets, '[0]._id');
        let shares = await Share.find({$or: [{userId}, {email: _.get(_.find(req.user.emails, email => email.type == 'account'), 'value')}]}).lean().exec();
        await Promise.join(
            Account.find({userId}, {key: 0}).lean().exec(),
            Transaction.find({userId, budgetId: currentBudgetId}).sort({DTPOSTED: -1}).lean().exec(),
            Folder.find({userId}).sort({order: 1}).lean().exec(),
            Rule.find({userId}).lean().exec(),
            BudgetProfile.find({userId}).lean().exec(),
            User.find({id: {$in: _.map(shares, share => share.userId)}}),
            (accounts, transactions, folders, rules, budgetProfiles, users) => {
                const hasBudgets = !_.isEmpty(budgets);
                budgets = _.keyBy(budgets, '_id');
                folders = _.keyBy(folders, '_id');
                rules = _.keyBy(rules, '_id');
                budgetProfiles = _.keyBy(budgetProfiles, '_id');
                users = _.keyBy(users, 'id');
                shares = _.map(shares, share => {
                    share.user = users[share.userId];
                    return share;
                });
                shares = _.keyBy(shares, '_id');
                transactions = _.groupBy(transactions, 'folderId');

                _.each(folders, (folder, folderId) => {
                    if(hasBudgets) {
                        folder.amount = budgets[currentBudgetId].folderAmounts[folderId];
                    }
                    let arrTransactions = transactions[folderId];
                    folder.transactions = arrTransactions;
                    const amount = _.round(_.sumBy(arrTransactions, 'TRNAMT'), 2);

                    // * Splits isn't accumulated nor used to calculate overall worth because the amounts are split into multiple transactions in different folders
                    if(folder.name == 'Splits') {
                        folder.amount = amount;
                    } else {
                        folder.amount = _.round(folder.amount + amount, 2);
                    }

                    // * hide transactions
                    folder.transactions = _.map(arrTransactions, transaction => {
                        if(transaction.hide && req.query.selectedUserId) {
                            transaction.NAME = 0;
                            transaction.NAME = 'Mr. Bacon: Hiding this transaction';
                            transaction.FITID = 'Mr. Bacon: Hiding this transaction';
                            transaction.DTPOSTED = 20170101000000;
                        }

                        return transaction;
                    });
                });

                res.json({
                    user: req.user,
                    accounts,
                    folders,
                    rules,
                    budgetProfiles,
                    budgets,
                    shares,
                    banks: _.keyBy(supportedBanks, 'formId')
                });
            }
        );
    } catch(err) {
        console.error(err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: "Looks like there was a problem getting your account, transactions, and rules.", form: 'wellsfargo'});
    }
};
exports.account = account;

exports.wellsfargo = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    // * get the credentials and encrypt them with the same key
    const username = req.body.username;
    const password = req.body.password;
    const encUsername = AES.encrypt(username);
    const key = encUsername.key;
    const encPassword = AES.encrypt(password, key);
    const encCredentials = {
        username: encUsername.content,
        password: encPassword.content
    };

    // * fetches the transactions and balance, but is used to verify we logged in successfully in this case
    try {
        await banks.wellsfargo(req, res, {username, password});
    } catch (err) {
        console.log('wellsfargo error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem fetching your transactions', form: 'wellsfargo'});
    }

    // * save the new account
    let account = {
        userId,
        bank: 'wellsfargo',
        key: key,
    };
    try {
        await new Account(account).save();
    } catch(err) {
        console.log('wellsfargo error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem updating your account', form: 'wellsfargo'});
    }

    // * send the encrypted credentials back to the user
    if(!res.headersSent) {
        console.log('success', 'wellsfargo');
        res.status(200).send({status: 'success', form: 'wellsfargo', encCredentials});
    }
};

banks.wellsfargo = async (req, res, credentials) => {
    const userId = req.query.selectedUserId || req.user.id;
    const username = credentials.username;
    const password = credentials.password;

    // * navigate to wells fargo website, login, attempt to skip ad, navigate to account page, scrape table of transactions (last 90 days)
    let result;
    try {
        const browser = await puppeteer.launch({args: ['--no-sandbox']});
        const page = await browser.newPage();
        page.on('console', msg => {
            for (let i = 0; i < msg.args().length; ++i)
                console.log(msg.args()[i].toString());
        });
        await page.goto('https://www.wellsfargo.com/');
        await page.type('#userid', username, {delay: 100});
        await page.type('#password', password, {delay: 100});
        await page.click('#frmSignon [type=submit]');
        await page.waitForNavigation({waitUntil: 'networkidle0'});

        try {
            await page.waitForSelector('a.secondary.samloutbound', {timeout: 6000});
            await page.click('a.secondary.samloutbound')
        } catch (e) {
            console.warn(e);
        }

        try {
            await page.waitForSelector('span.account-name', {timeout: 5000});
        } catch (e) {
            console.warn(e);
            await page.screenshot({path: 'wellsfargo_error.png'});
            let htmlError = await page.content();
            fs.writeFile('wellsfargo_error.html', htmlError);
        }

        result = await page.evaluate(userId => {
            return new Promise(function(resolve, reject) {
                function reqListener() {
                    let accountDetailsString = this.responseText;
                    accountDetails = JSON.parse(accountDetailsString.replace('"/*WellFargoProprietary%', '').replace('%WellFargoProprietary*/"', ''));
                    let accountDetailsParser = new DOMParser();
                    let preDocument = accountDetailsParser.parseFromString(accountDetails.htmlResponse, 'text/html');
                    let document = accountDetailsParser.parseFromString(preDocument.body.textContent, 'text/html');

                    let spaceParser = new DOMParser();
                    let doc = spaceParser.parseFromString('&nbsp;', 'text/html');
                    let decodedSpaceString = doc.body.textContent;

                    let data = { balance: parseFloat(document.querySelector('span.account-balance').innerText.replace(/\$|\,/g, '')) };
                    let transactions = { pending: [], posted: [] };
                    let pending = true;
                    Array.from(document.querySelectorAll('table.transaction-expand-collapse > tbody > tr.detailed-transaction')).forEach((elm, row) => {
                        let transaction = {};
                        Array.from(elm.querySelectorAll('td')).forEach((colElm, col) => {
                            if (col === 0) {
                                if (colElm.attributes.headers.value.includes('posted-trans')) {
                                    pending = false;
                                }
                            } else if (col === 1) {
                                date = new Date(colElm.innerText).toISOString().replace(/-|T|:/g, '');
                                date = date.substring(0, 8) + "050000";
                                date = parseFloat(date);
                                transaction.DTPOSTED = date;
                            } else if (col === 2) {
                                transaction.NAME = colElm.innerText.trim().replace(/\s\s+/g, ' ');
                            } else if (col === 3 && colElm.innerText !== decodedSpaceString) {
                                let amount = colElm.innerText.replace(/\$|\,/g, '');
                                amount = parseFloat(amount);
                                transaction.TRNAMT = amount;
                            } else if (col === 4 && colElm.innerText !== decodedSpaceString) {
                                let amount = colElm.innerText.replace(/\$|\,/g, '');
                                amount = parseFloat(amount);
                                amount *= -1;
                                transaction.TRNAMT = amount;
                            }
                        });

                        transaction.FITID = transaction.DTPOSTED + transaction.TRNAMT + transaction.NAME;
                        transaction.SIC = -1;
                        transaction.TRNTYPE = '';
                        transaction.userId = userId;
                        transaction.bank = 'wellsfargo';
                        transaction.type = 'bank';
                        if (pending) {
                            transactions.pending.push(transaction)
                        } else {
                            transactions.posted.push(transaction);
                        }
                    });

                    data.transactions = transactions;
                    resolve(data);
                }

                var oReq = new XMLHttpRequest();
                oReq.addEventListener("load", reqListener);
                // oReq.open("GET", document.querySelector('a.view-activity').attributes["data-url"].value);
                oReq.open("GET", document.querySelector('.account-tile-header > a.account-title-group').attributes["data-url"].value);
                oReq.send();
            });
        }, userId);
        await browser.close();
    } catch(err) {
        console.log('wellsfargo error:', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem connecting to your account.', form: 'wellsfargo'});
    }

    // * there might be a chance you have a pair or more of transactions with the same date, name, and amount. If we find that case loop over the conflicts and add an incrementing number to the end of the FITID;
    let conflicts = _.pickBy(_.groupBy(result.transactions.posted, 'FITID'), g => g.length > 1);
    if(_.keys(conflicts).length > 0) {
        conflictsCount = {};
        result.transactions.posted.map(transaction => {
            if(_.has(conflicts, transaction.FITID)) {
                if(!_.has(conflictsCount, transaction.FITID)) {
                    conflictsCount[transaction.FITID] = 0;
                }
                transaction.FITID += conflictsCount[transaction.FITID].toString();
            }
            return transaction;
        });
    }

    // * format result into something the sync function can use. we don't use pending transactions yet
    result = {transactions: result.transactions.posted, balance: result.balance};
    // fs.writeFile('wf_data.js', 'module.exports = ' + JSON.stringify(result, null, '  '));

    // // * for testing
    // result = require('./wf_data');

    // * update account balance if available
    try {
        await Account.findOneAndUpdate({userId, bank: 'wellsfargo'}, {balance: result.balance}, {safe: true}).exec()
    } catch(err) {
        console.log('wellsfargo error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem updating your account', form: 'wellsfargo'});
    }

    return Promise.resolve(result);
};

const pad0 = val => {
    return (val < 10 && val >= 0 ? "0" + val : val);
}

const USAA_QWIN = {
    fid: 24591,
    fidOrg: 'USAA',
    url: 'https://service2.usaa.com/ofx/OFXServlet',
    ofxVer: 102,
    app: 'QWIN',
    appVer: '2400'
};

exports.usaa = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    // * get the credentials and encrypt them with the same key
    const user = req.body.user;
    const password = req.body.password;
    const accId = req.body.accId;
    const accType = req.body.accType;
    // const bankId = req.body.bankId;
    const encUser = AES.encrypt(user);
    const key = encUser.key;
    const encPassword = AES.encrypt(password, key);
    const encAccId = AES.encrypt(accId, key);
    const encAccType = AES.encrypt(accType, key);
    // const encBankId = AES.encrypt(req.body.bankId, user.key);
    const encCredentials = {
        user: encUser.content,
        password: encPassword.content,
        accId: encAccId.content,
        accType: encAccType.content
        // bankId: encBankId.content
    };
    const accountCreds = Object.assign({}, req.body, USAA_QWIN);

    // * fetches the transactions and balance, but is used to verify we logged in successfully in this case
    await banks.usaa(req, res, {user, password, accId, accType, /* bankId */});

    // * save the new account
    let account = {
        userId,
        bank: 'usaa',
        key,
    };
    try {
        await new Account(account).save();
    } catch(err) {
        console.log('usaa error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem creating your account', form: 'usaa'});
    }

    // * send the encrypted credentials back to the user
    if(!res.headersSent) {
        console.log('success', 'usaa');
        res.status(200).send({status: 'success', form: 'usaa', encCredentials});
    }
};

banks.usaa = async (req, res, credentials) => {
    const userId = req.query.selectedUserId || req.user.id;

    let date = new Date();
    let todaysDate = "" + pad0(date.getFullYear()) + pad0(date.getMonth()+1) + pad0(date.getDate());

    // * build credentials for request to the OFX api
    const user = credentials.user;
    const password = credentials.password;
    const accId = credentials.accId;
    const accType = credentials.accType;
    // const bankId = credentials.bankId;
    const accountCreds = Object.assign({}, credentials, USAA_QWIN);
    const bank = Banking(accountCreds);
    const getStatementAsync = Promise.promisify(bank.getStatement, {context: bank});
    let data;

    // * send request for account/transaction data between 01/01/2017 and today
    let transactions;
    try {
        transactions = await getStatementAsync({start: 20170101, end: todaysDate});
    } catch(err) {
        console.log('usaa error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem connecting your account', form: 'usaa'});
    }

    // * format result into something the sync function can use. we don't use pending transactions yet
    data = {
        balance: transactions.body.OFX.CREDITCARDMSGSRSV1.CCSTMTTRNRS.CCSTMTRS.LEDGERBAL.BALAMT,
        transactions: _.map(
            transactions.body.OFX.CREDITCARDMSGSRSV1.CCSTMTTRNRS.CCSTMTRS.BANKTRANLIST.STMTTRN,
            item => {
                item.userId = userId;
                item.bank = 'usaa';
                item.type = 'bank';
                return item;
            }
        )
    };

    // fs.writeFile('usaa_data.js', 'module.exports = ' + JSON.stringify(data, null, '  '));

    // // * for testing purposes
    // data = require('./usaa_data');


    // * update account balance if available
    try {
        await Account.findOneAndUpdate({userId, bank: 'usaa'}, {balance: data.balance}, {safe: true}).exec()
    } catch(err) {
        console.log('usaa error: ', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem updating your account', form: 'usaa'});
    }

    return Promise.resolve(data);
};

const insertAccountData = (req, res, data) => {
    data = _.sortBy(data, ['DTPOSTED']);

    const userId = req.query.selectedUserId || req.user.id;

    // * find already stored transactions
    const mapTransactionsPromise = Promise.mapSeries(data, transaction => {
        return Transaction.findOne({userId, FITID: transaction.FITID}).lean().exec();
    });

    return Promise.join(
        Folder.find({userId}).lean().exec(),
        Rule.find({userId}).lean().exec(),
        Budget.findOne({userId}).sort({date: -1}).lean().exec(),
        mapTransactionsPromise,
        async (folders, rules, budget, foundTransactions) => {
            folders = _.keyBy(folders, '_id');
            let currentBudgetId = budget ? budget._id : null;
            if (currentBudgetId) _.each(folders, (folder, folderId) => folder.amount = budget.folderAmounts[folderId]);

            for(let [index, transaction] of data.entries()) {
                if(res.headersSent) break;
                if(foundTransactions[index] && !req.query.selectedTransactionId) continue;

                const bank = transaction.bank;
                const inboxFolder = _.find(folders, folder => folder.name == 'Inbox');
                const savingsFolder = _.find(folders, folder => folder.name == 'Savings');

                // * find which folder this transaction belongs in or if it's a paycheck
                // TODO: make this a get rule function so it's cleaner
                const sorted = sortTransaction(transaction, folders, rules, userId);

                // * if it belongs in a folder or is the paycheck, but budgeting hasn't started, assign that folder to this transaction and save it
                if (sorted._id || (sorted.isPaycheck && !currentBudgetId)) {
                    if (!currentBudgetId) sorted._id = inboxFolder._id;
                    try {
                        const transactionForInsert = Object.assign({}, transaction, {
                            folderId: sorted._id,
                            budgetId: currentBudgetId,
                            read: false,
                            hide: false,
                        });
                        await Transaction.update({ FITID: transaction.FITID, userId }, transactionForInsert, { safe: true, upsert: true, new: true }).exec();
                    } catch(err)  {
                        console.log(bank, "transaction error:", err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: "Looks like there was a problem saving transactions.", form: bank});
                    }
                // * if it's a paycheck we do a few things
                } else if(sorted.isPaycheck) {
                    // * get the updated list of transactions and the budget profile associated with this paycheck
                    let transactionsPrePaycheck;
                    let currentBudgetProfile;
                    try{
                        transactionsPrePaycheck = await Transaction.find({userId, budgetId: currentBudgetId}).lean().exec();
                        currentBudgetProfile = await BudgetProfile.findOne({_id: sorted.rule.budgetProfileId, userId}).lean().exec();
                    } catch(e) {
                        console.log(bank, 'budget error:', err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem getting saved transactions or the current budget profile.', form: bank});
                    }

                    // * Move inbox transactions into savings
                    try {
                        let promises = [];
                        transactionsPrePaycheck = _.map(transactionsPrePaycheck, transaction => {
                            if(transaction.folderId == inboxFolder._id) transaction.folderId = savingsFolder._id;

                            promises.push(Transaction.findByIdAndUpdate(transaction._id, transaction).exec());

                            return transaction;
                        });
                        for(let promise of promises) {
                            await promise;
                        }
                    } catch(err) {
                        console.log(bank, 'budget error:', err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving the budget.', form: bank});
                    }

                    // * update the list of transactions and amount for each folder
                    transactionsPrePaycheck = _.groupBy(transactionsPrePaycheck, 'folderId');
                    for(let [folderId, folder] of Object.entries(folders)) {
                        const arrTransactions = transactionsPrePaycheck[folderId];
                        folder.transactions = arrTransactions;
                        if(_.get(currentBudgetProfile, `divisions.${folderId}.transfer`)) {
                            try {
                                let amount = folders[folderId].amount + _.sumBy(arrTransactions, 'TRNAMT');
                                let savingsTransferTransaction = await new Transaction({
                                    userId,
                                    bank: 'mrbacon',
                                    NAME: `Mr. Bacon Transfer: ${folders[folderId].name}`,
                                    SIC: -1,
                                    FITID: `Mr. Bacon Transfer: ${folders[folderId].name} ${transaction.DTPOSTED}`,
                                    TRNAMT: amount,
                                    DTPOSTED: transaction.DTPOSTED,
                                    TRNTYPE: null,
                                    folderId: savingsFolder._id,
                                    budgetId: currentBudgetId,
                                    read: false,
                                    hide: false,
                                    posted: true,
                                    linkedTransactionId: null,
                                    type: 'budget_transfer'
                                }).save();
                                let folderTransferTransaction = await new Transaction({
                                    userId,
                                    bank: 'mrbacon',
                                    NAME: `Mr. Bacon Transfer: Savings`,
                                    SIC: -1,
                                    FITID: `Mr. Bacon Transfer: Savings ${transaction.DTPOSTED}`,
                                    TRNAMT: -amount,
                                    DTPOSTED: transaction.DTPOSTED,
                                    TRNTYPE: null,
                                    folderId: folderId,
                                    budgetId: currentBudgetId,
                                    read: false,
                                    hide: false,
                                    posted: true,
                                    linkedTransactionId: savingsTransferTransaction._id,
                                    type: 'budget_transfer'
                                }).save();
                                folders[savingsFolder._id].amount += amount;
                                folders[folderId].amount = 0;
                            } catch (err) {
                                console.log(bank, 'budget error:', err);
                                if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem balancing the folders.', form: bank});
                            }
                        } else {
                            folder.amount += _.sumBy(arrTransactions, 'TRNAMT');
                        }
                    }

                    // * create a new budget starting the day the paycheck cleared and save it.
                    // * save the starting amounts for each folder to the budget
                    // * all future transactions will be associated with this budgetId
                    const budgetData = {
                        userId,
                        date: transaction.DTPOSTED,
                        budgetProfileId: sorted.rule.budgetProfileId,
                        folderAmounts: _.mapValues(folders, folder => folder.amount)
                    };
                    try {
                        let newBudget = await new Budget(budgetData).save();
                        currentBudgetId = newBudget._id;
                    } catch(err) {
                        console.log(bank, 'budget error:', err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving the budget.', form: bank});
                    }

                    const transactionForInsert = Object.assign({}, transaction, {
                        folderId: _.find(folders, folder => folder.name == 'Splits')._id,
                        budgetId: currentBudgetId,
                        read: false,
                        hide: false,
                        type: 'paycheck'
                    });
                    let savedPaycheck;
                    try {
                        savedPaycheck = await new Transaction(transactionForInsert).save();
                    } catch(err) {
                        console.log(bank, 'transaction error:', err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving paycheck transactions.', form: bank});
                    }

                    // * distribute the paycheck based on the budget profile
                    try {
                        let budgetProfile = await BudgetProfile.findById(budgetData.budgetProfileId).lean().exec();
                        const divisions = budgetProfile.divisions;
                        if(!_.find(divisions, division => division.amount == 'remaining')) {
                            division[savingsFolder._id].amount = 'remaining';
                        }
                        const divisionsSum = _.chain(divisions).values().map('amount').filter(_.isNumber).sum().value();

                        if(divisionsSum > transaction.TRNAMT) {
                            // ! handle this somehow
                            console.log(bank, 'paycheck division error:', err);
                            if(!res.headersSent) res.status(500).send({status: 'error', message: `Your budget profile assumed a paycheck amount of at least ${divisionsSum} but your paycheck was only ${transaction.TRNAMT}.`, form: bank});
                        } else {
                            for(let [folderId, division] of Object.entries(divisions)) {
                                const paycheckTransaction = Object.assign({}, transactionForInsert, {
                                    type: 'paycheck',
                                    bank,
                                    folderId,
                                    TRNAMT: division.amount == 'remaining' ? transaction.TRNAMT - divisionsSum : division.amount,
                                    linkedTransactionId: savedPaycheck._id
                                });

                                try {
                                    await new Transaction(paycheckTransaction).save();
                                } catch(err) {
                                    console.log(bank, 'transaction error:', err);
                                    if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving split paycheck transaction.', form: bank});
                                }
                            }
                        }
                    } catch(err) {
                        console.log("budget error:", err);
                        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem getting budget.'});
                    }
                } else {
                    console.log(bank, "folder error:", err);
                    if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem getting a folder.', form: bank});
                }
            }
        }
    );
}

// TODO: possibly make existing transactions affected by new rules
const sortTransaction = (transaction, folders, rules, userId) => {
    let name = transaction.NAME.toLowerCase();
    let amount = transaction.TRNAMT;
    let ruleFound = null;

    _.some(rules, rule => {
        let success = true;
        _.every(rule.parameters, parameter => {
            const expectedIndex = name.length - parameter.val.length;
            const parameterVal = typeof parameter.val == "string" ? parameter.val.toLowerCase() : parameter.val;

            if(parameter.field == "starts" && ((!parameter.not && name.indexOf(parameterVal) != 0) || (parameter.not && name.indexOf(parameterVal) == 0))) {
                success = false;
                return false;
            } else if(parameter.field == "contains" && ((!parameter.not && name.indexOf(parameterVal) == -1) || (parameter.not && name.indexOf(parameterVal) != -1))) {
                success = false;
                return false;
            } else if(parameter.field == "ends" && ((!parameter.not && name.indexOf(parameterVal) != expectedIndex) || (parameter.not && name.indexOf(parameterVal) == expectedIndex))) {
                success = false;
                return false;
            } else if (parameter.field == "amount" && ((!parameter.not && amount != parameterVal) || (parameter.not && amount == parameterVal))) {
                success = false;
                return false;
            } else if (parameter.field == "amountMin" && ((!parameter.not && amount < parameterVal) || (parameter.not && amount >= parameterVal))) {
                success = false;
                return false;
            } else if (parameter.field == "amountMax" && ((!parameter.not && amount > parameterVal) || (parameter.not && amount <= parameterVal))) {
                success = false;
                return false;
            }

            return true;
        });

        if(success) {
            ruleFound = rule;
            if(rule.isPaycheck) return true;
        }

        return false;
    });

    ruleFound = ruleFound || {folderId: _.find(folders, folder => folder.name == 'Inbox')._id};
    if (ruleFound.isPaycheck) {
        return {_id: false, isPaycheck: true, rule: ruleFound};
    } else {
        return folders[ruleFound.folderId];
    }
}

exports.sync = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    // * get accounts to sync
    let accounts;
    try {
        accounts = await Account.find({userId}).lean().exec();
    } catch(e) {
        console.log("Sync Error:", e);
        res.status(500).send({status: 'error', message: "Looks like there was a problem saving your account."});
    }

    const encCredentialsArr = JSON.parse(req.body.encCredentials);
    let allTransactions = [];
    let promises = [];

    // * collect data for each account
    for(let account of accounts) {
        if(res.headersSent) break;

        let credentials = {};
        const encCredentials = _.find(encCredentialsArr, creds => creds.form == account.bank).encCredentials;
        const key = account.key;

        // * decrypt encrypted credentials to use during login
        if(AES.decrypt(encCredentials[Object.keys(encCredentials)[0]], key) && !res.headersSent) {
            _.each(encCredentials, (v, cred) => {
                credentials[cred] = AES.decrypt(v, key);
            });
            promises.push(banks[account.bank](req, res, credentials));
        } else {
            console.log('Sync Error:', e);
            if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem decrypting your credentials.'});
        }
    }

    // * update the database with all the new transaction info
    try {
        for(let promise of promises) {
            let bankData = await promise;
            allTransactions = allTransactions.concat(bankData.transactions);
        }
        await insertAccountData(req, res, allTransactions);
    } catch(e) {
        console.log("Sync Error:", e);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving all your transactions.'});
    }

    return account(req, res);
};

exports.newFolder = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    let folder = new Folder({
        userId,
        name: req.body.name,
        amount: req.body.amount,
        order: req.body.order,
        icon: req.body.icon,
        permissions: {
            change: true,
            move: true,
            budgetProfile: true
        }
    });

    try {
        folder = await folder.save();
        let budgets = await Budget.find({userId}).sort({date: -1}).lean().exec();
        for(let budget of budgets) {
            budget.folderAmounts[folder._id] = req.body.amount;
            await Budget.findByIdAndUpdate(budget._id, budget);
        }
        return account(req, res);
    } catch(err) {
        console.log("New folder error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem creating a folder."});
    }
}

exports.editFolder = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const folder = {
        name: req.body.name,
        // order: req.body.order,
        icon: req.body.icon
    };

    Folder.findOneAndUpdate({_id: req.params.id, userId}, folder, {safe: true, upsert: true, new : true}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log('Edit folder error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem editing a folder.'});
    });
}

exports.removeFolder = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    let removedFolder = await Folder.findById(req.params.id).lean();
    let savingsFolder = await Folder.findOne({userId, name: 'Savings'}).lean().exec();
    let folderTransactions = await Transaction.find({userId, folderId: req.params.id}).lean().exec();
    let budgets = await Budget.find({userId}).sort({date: -1}).lean().exec();
    let latestBudget = budgets[0];
    let firstBudget = budgets[budgets.length - 1];

    removedFolder.amount = latestBudget.folderAmounts[removedFolder._id] + _.sumBy(folderTransactions, 'TRNAMT');
    await transferAmount(userId, latestBudget._id, removedFolder._id, req.savingsFolder._id, removedFolder.amount);
    folderTransactions = await Transaction.find({userId, folderId: req.params.id}).lean().exec();

    let {folderBankTransactions, folderPaycheckTransactions, folderBudgetTransferTransactions, folderSplitTransactions, folderTransferTransactions} = _.groupBy(folderTransactions, transaction => {
        if(transaction.type == 'budget_transfer') {
            return 'folderBudgetTransferTransactions';
        } else if(transaction.type == 'paycheck') {
            return 'folderPaycheckTransactions';
        } else if (transaction.type == 'split') {
            return 'folderSplitTransactions';
        } else if(transaction.type == 'transfer') {
            return 'folderTransferTransactions';
        }

        return 'folderBankTransactions';
    });

    if(folderPaycheckTransactions) {
        for(let transaction of folderPaycheckTransactions) {
            let savingsPaycheckTransaction = await Transaction.findOne({userId, folderId: savingsFolder._id, type: 'paycheck', linkedTransactionId: transaction.linkedTransactionId}).lean().exec();
            if(savingsPaycheckTransaction) {
                savingsPaycheckTransaction.TRNAMT += transaction.TRNAMT;
            }
            try {
                await Transaction.findByIdAndUpdate(savingsPaycheckTransaction._id, savingsPaycheckTransaction);
                await Transaction.findByIdAndRemove(transaction._id);
            } catch(err) {
                console.log('Remove folder error:', err);
                res.status(500).send({status: 'error', message: 'Looks like there was a problem removing or updating a paycheck transaction.'});
            }
        }
    }

    for(let transaction of folderBudgetTransferTransactions) {
        let transactionIds = [];
        transactionIds.push(transaction._id);
        transactionIds.push(transaction.linkedTransactionId);
        await removeTransactions(transactionIds, userId, res);
    }

    for(let transaction of folderTransferTransactions) {
        let transactionIds = [];
        transactionIds.push(transaction._id);
        // transactionIds.push(transaction.linkedTransactionId);
        await removeTransactions(transactionIds, userId, res);
    }

    for(let [budgetId, transactions] of Object.entries(_.groupBy(folderSplitTransactions, 'budgetId'))) {
        let transactionIds = _.map(transactions, '_id');
        await moveTransactions(userId, transactionIds, savingsFolder._id, budgetId, res);
    }

    for(let [budgetId, transactions] of Object.entries(_.groupBy(folderBankTransactions, 'budgetId'))) {
        let transactionIds = _.map(transactions, '_id');
        await moveTransactions(userId, transactionIds, savingsFolder._id, budgetId, res);
    }

    await Rule.remove({userId, folderId: req.body.folderId}).lean().exec();

    let budgetProfiles = await BudgetProfile.find({userId, [`division.${req.params.id}`]: {$exists: true}}).lean().exec();

    for(let budgetProfile of budgetProfiles) {
        delete budgetProfile.division[req.params.id];
        await BudgetProfile.findByIdAndUpdate(budgetProfile._id, budgetProfile);
    }

    if(removedFolder.amount != 0) {
        savingsFolder.amount += removedFolder.amount;
        await Folder.findByIdAndUpdate(savingsFolder._id, savingsFolder);
    }

    await Folder.findByIdAndRemove(req.params.id);

    return recalculateBudgets(userId, firstBudget._id)
    return account(req, res);
}

exports.newRule = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const rule = new Rule({
        userId,
        name: req.body.name,
        folderId: req.body.folderId,
        parameters: req.body.parameters,
        isPaycheck: req.body.isPaycheck,
        budgetProfileId: req.body.budgetProfileId
    });

    rule.save().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("New rule error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem creating a folder."});
    });
}

exports.editRule = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const rule = {
        userId,
        name: req.body.name,
        folderId: req.body.folderId,
        parameters: req.body.parameters,
        isPaycheck: req.body.isPaycheck,
        budgetProfileId: req.body.budgetProfileId
    };

    Rule.findOneAndUpdate({_id: req.params.id, userId}, rule, {safe: true, upsert: true, new : true}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("Edit rule error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem editing a rule."});
    });
}

exports.removeRule = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    Rule.findOneAndRemove({_id: req.params.id, userId}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("Remove rule error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem removing a rule."});
    });
}

exports.newBudgetProfile = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const budgetProfile = new BudgetProfile({
        userId,
        name: req.body.name,
        divisions: req.body.divisions,
        divisionUnits: req.body.divisionUnits
    });

    budgetProfile.save().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("New budget profile error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem creating a budget profile."});
    });
}

exports.editBudgetProfile = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const budgetProfile = {
        userId,
        name: req.body.name,
        divisions: req.body.divisions,
        divisionUnits: req.body.divisionUnits
    };

    BudgetProfile.findOneAndUpdate({_id: req.params.id, userId}, budgetProfile, {safe: true, upsert: true, new : true}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("Edit budget profile error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem editing a budget profile."});
    });
}

exports.removeBudgetProfile = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    BudgetProfile.findOneAndRemove({_id: req.params.id, userId}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log("Remove rule error:", err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem removing a rule."});
    });
}

exports.newBudget = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    // * if there isn't a starting budget, make one. Delete starting transaction and future transactions
    try {
        let startingBudget = await Budget.findOne({userId}).sort({date: -1}).lean().exec();
        if(!startingBudget) {
            let folders = await Folder.find({userId}).lean().exec();
            folders = _.keyBy(folders, '_id');
            const firstBudget = new Budget({
                userId,
                date: 20170101000000,
                budgetProfileName: '',
                folderAmounts: _.mapValues(folders, folder => folder.amount)
            });
            await firstBudget.save();

            const transactions = await Transaction.find({userId}).sort({DTPOSTED: -1}).lean().exec();
            const selectedTransactionIndex = _.findIndex(transactions, transaction => transaction._id == req.query.selectedTransactionId);
            let transactionsToBeSorted = _.take(transactions, selectedTransactionIndex + 1);
            transactionsToBeSorted = _.map(transactions, transaction => {
                delete transaction._id;
                return transaction;
            })
            await insertAccountData(req, res, transactionsToBeSorted);

            return account(req, res);
        } else {
            console.log('New budget error:', err);
            res.status(412).send({status: 'error', message: 'A starting budget already exists.'});
        }
    } catch(err) {
        console.log('New budget error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem creating a budget.'});
    };
}

const recalculateBudgets = async (userId, budgetId) => {
    try {
        // * if we are in a past budget, we have to recalculate future folder and transfer transactions amounts
        if(budgetId) {
            let folders = await Folder.find({userId}).lean().exec();
            folders = _.keyBy(folders, '_id');
            const savingsFolder = _.find(folders, folder => folder.name == 'Savings');
            let budgets = await Budget.find({userId}).sort({date: 1}).lean().exec();
            let selectedBudgetDate = _.keyBy(budgets, '_id')[budgetId].date;
            budgets = _.filter(budgets, budget => budget.date >= selectedBudgetDate);
            // let budgetProfiles = await BudgetProfile.find({userId}).lean().exec();
            // budgetProfiles = _.keyBy(budgetProfiles, '_id');

            // * for each budget get the previous budget's transactions and recalculate the folder and transfer transactions amounts
            for(let [index, budget] of budgets.entries()) {
                if(index == 0) continue;

                // * get transactions of previous budget
                let transactions = await Transaction.find({userId, budgetId: budgets[index - 1]._id}).lean().exec();
                transactions = _.groupBy(transactions, 'folderId');

                // * for each folder recalculate the folder using the folder's transaction from the previous budget and recaluclate any transfer transactions amounts
                for(let [folderId, folder] of Object.entries(folders)) {
                    const arrTransactions = transactions[folderId];

                    // * calculate new folder amount
                    let amount = budgets[index - 1].folderAmounts[folderId] + _.round(_.sumBy(arrTransactions, 'TRNAMT'), 2);

                    // * make sure there are transfer transactions
                    let folderTransferTransaction = _.find(arrTransactions, transaction => transaction.type == 'budget_transfer');
                    let savingsTransferTransaction = _.find(transactions[savingsFolder._id], transaction => folderTransferTransaction && transaction._id == folderTransferTransaction.linkedTransactionId);

                    // * if a budget_transfer transaction exists, and the new folder amount isn't 0, then recalculate the budget_transfer transaction
                    if(folderTransferTransaction && savingsTransferTransaction && amount != 0) {
                        savingsTransferTransaction.TRNAMT += amount;
                        folderTransferTransaction.TRNAMT -= amount;
                        await Transaction.findByIdAndUpdate(savingsTransferTransaction._id, savingsTransferTransaction);
                        await Transaction.findByIdAndUpdate(folderTransferTransaction._id, folderTransferTransaction);
                        budget.folderAmounts[savingsFolder._id] += amount;
                        amount = 0;
                    }

                    budget.folderAmounts[folderId] = amount;
                }
                await Budget.findOneAndUpdate({_id: budget._id, userId}, {$set: {folderAmounts: budget.folderAmounts}}, {safe: true}).exec();
            }
        }
    } catch (err) {
        console.log('recalculate budgets error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem recalculating budgets.'});
    }
}

const removeTransactions = async(transactionIds, selectedBudgetId, userId, res) => {
    try {
        await Transaction.remove({_id: {$in: transactionIds}, userId}).exec();
        await recalculateBudgets(userId, selectedBudgetId);
    } catch(err) {
        console.log('Remove transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem removing a transaction.'});
    }
};

exports.removeTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        // let startingBudget = await Budget.findOne({userId}).sort({date: -1}).lean().exec();
        // if(!startingBudget) {
            await removeTransactions(req.body.transactionIds, req.query.selectedBudgetId, userId, res);
            return account(req, res);
        // } else {
        //     console.log('New budget error: A starting budget already exists.');
        //     res.status(412).send({status: 'error', message: 'A starting budget already exists.'});
        // }
    } catch(err) {
        console.log('Remove transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem removing a transaction.'});
    }
}

const searchTransactions = (query, userId, res) => {
    let amountQuery = {TRNAMT: query.search};

    if (isNaN(query.search)) {
        amountQuery = {NAME: {$regex: query.search, $options: 'i'}};
    }

    return Transaction.find({$or: [{NAME: {$regex: query.search, $options: 'i'}}, amountQuery], userId}).sort({DTPOSTED: -1}).lean().exec();
};

exports.searchTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        let transactions = await searchTransactions(req.query, userId, res);
        return res.json(transactions);
    } catch(err) {
        console.log('Remove transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem removing a transaction.'});
    }
}

const moveTransactions = async (userId, transactionIds, folderId, budgetId, res) => {
    try {
        // * move transactions to new folder
        await Transaction.update({_id: {$in: transactionIds}, userId}, {$set: {folderId}}, {safe: true, multi: true}).exec();
        await recalculateBudgets(userId, budgetId, res);
    } catch(err) {
        console.log('Move transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem moving transactions.'});
    }
};

exports.moveTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await moveTransactions(userId, req.body.transactionIds, req.body.folderId, req.query.selectedBudgetId, res);
    } catch(err) {
        console.log('Move transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem moving transactions.'});
    }
    return account(req, res);
};

const moveBudgetTransactions = async (userId, transactionIds, newBudgetId, budgetId, res) => {
    try {
        // * move transactions to new budget
        await Transaction.update({_id: {$in: transactionIds}, userId}, {$set: {budgetId: newBudgetId}}, {safe: true, multi: true}).exec();
        let newBudget = await Budget.findOne({_id: newBudgetId, userId}).lean().exec();
        let budget = await Budget.findOne({_id: budgetId, userId}).lean().exec();
        await recalculateBudgets(userId, budget.date < newBudget.date ? budgetId : newBudgetId, res);
    } catch (err) {
        console.log('Change budget transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem moving transactions to budget.'});
    }
};

exports.moveBudgetTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await moveBudgetTransactions(userId, req.body.transactionIds, req.body.budgetId, req.query.selectedBudgetId, res);
    } catch (err) {
        console.log('Change budget transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem moving transactions to budget.'});
    }
    return account(req, res);
};

exports.exportData = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await Promise.join(
            // User.find({id: userId}, {__v: 0}).lean().exec(),
            Account.find({userId}, {__v: 0}).lean().exec(),
            Transaction.find({userId}, {__v: 0}).lean().exec(),
            Folder.find({userId}, {__v: 0}).lean().exec(),
            Rule.find({userId}, {__v: 0}).lean().exec(),
            BudgetProfile.find({userId}, {__v: 0}).lean().exec(),
            Budget.find({userId}, {__v: 0}).lean().exec(),
            Share.find({userId}, {__v: 0}).lean().exec(),
            (/* users, */ accounts, transactions, folders, rules, budgetProfiles, budgets, shares) => {
                res.json({
                    // users,
                    accounts,
                    transactions,
                    folders,
                    rules,
                    budgetProfiles,
                    budgets,
                    shares
                });
            }
        );
    } catch(err) {
        console.error(err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem getting your account, transactions, and rules.', form: 'wellsfargo'});
    }
}

exports.fixData = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        let folders = await Folder.find({userId}).lean().exec();
        folders = _.keyBy(folders, '_id');
        const savingsFolder = _.find(folders, folder => folder.name == 'Savings');
        let budgets = await Budget.find({userId}).sort({date: 1}).lean().exec();

        // * for each budget get the previous budget's transactions and recalculate the folder and transfer transactions amounts
        for (let [index, budget] of budgets.entries()) {
            if (index == 0) continue;

            // * get transactions of previous budget
            let transactions = await Transaction.find({userId, budgetId: budgets[index - 1]._id}).lean().exec();
            transactions = _.groupBy(transactions, 'folderId');

            // * for each folder recalculate the folder using the folder's transaction from the previous budget and recaluclate any transfer transactions amounts
            for (let [folderId, folder] of Object.entries(folders)) {
                const arrTransactions = transactions[folderId];

                // * calculate new folder amount
                let amount = budgets[index - 1].folderAmounts[folderId] + _.round(_.sumBy(arrTransactions, 'TRNAMT'), 2);

                // * make sure there are transfer transactions
                let folderTransferTransaction = _.find(arrTransactions, transaction => transaction.type == 'budget_transfer');
                let savingsTransferTransaction = _.find(transactions[savingsFolder._id], transaction => folderTransferTransaction && transaction._id == folderTransferTransaction.linkedTransactionId);

                // * if a budget_transfer transaction exists, and the new folder amount isn't 0, then recalculate the budget_transfer transaction
                if (folderTransferTransaction && savingsTransferTransaction && amount != 0) {
                    savingsTransferTransaction.TRNAMT += amount;
                    folderTransferTransaction.TRNAMT -= amount;
                    await Transaction.findByIdAndUpdate(savingsTransferTransaction._id, savingsTransferTransaction);
                    await Transaction.findByIdAndUpdate(folderTransferTransaction._id, folderTransferTransaction);
                    budget.folderAmounts[savingsFolder._id] += amount;
                    amount = 0;
                }

                budget.folderAmounts[folderId] = amount;
            }
            await Budget.findOneAndUpdate({_id: budget._id, userId}, {$set: {folderAmounts: budget.folderAmounts}}, {safe: true}).exec();
        }
    } catch (err) {
        console.log('fix data error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem fixing data.'});
    }

    return account(req, res);
}

exports.importData = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        let promises = [];
        let removePromises = []
        // const users = req.body.exportedData.users;
        const accounts = req.body.exportedData.accounts;
        const transactions = req.body.exportedData.transactions;
        const folders = req.body.exportedData.folders;
        const rules = req.body.exportedData.rules;
        const budgetProfiles = req.body.exportedData.budgetProfiles;
        const budgets = req.body.exportedData.budgets;
        const shares = req.body.exportedData.shares;

        removePromises.push(Account.remove({}).exec());
        removePromises.push(Transaction.remove({}).exec());
        removePromises.push(Folder.remove({}).exec());
        removePromises.push(Rule.remove({}).exec());
        removePromises.push(BudgetProfile.remove({}).exec());
        removePromises.push(Budget.remove({}).exec());
        removePromises.push(Share.remove({}).exec());
        await Promise.all(removePromises);

        // _.each(users, user => promises.push(User.findOneAndUpdate({_id: user._id, id: userId}, user, {safe: true, upsert: true, new : true}).exec()));
        _.each(accounts, account => promises.push(new Account(account).save()));
        _.each(transactions, transaction => promises.push(new Transaction(transaction).save()));
        _.each(folders, folder => promises.push(new Folder(folder).save()));
        _.each(rules, rule => promises.push(new Rule(rule).save()));
        _.each(budgetProfiles, budgetProfile => promises.push(new BudgetProfile(budgetProfile).save()));
        _.each(budgets, budget => promises.push(new Budget(budget).save()));
        _.each(shares, share => promises.push(new Share(share).save()));

        await Promise.all(promises);
        return account(req, res);
    } catch(err) {
        console.error(err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem importing your data.'});
    }
}

exports.newShare = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    let share = new Share({
        userId,
        email: req.body.email
    });

    try {
        share = await share.save();
        return account(req, res);
    } catch(err) {
        console.log('New share error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem creating a share.'});
    }
}

exports.editShare = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    const share = {
        email: req.body.email
    };

    Share.findOneAndUpdate({_id: req.params.id, userId}, share, {safe: true, upsert: true, new : true}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log('Edit share error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem editing a share.'});
    });
}

exports.removeShare = (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    Share.findOneAndRemove({_id: req.params.id, userId}).exec().then(() => {
        return account(req, res);
    }).catch(err => {
        console.log('Remove rule error:', err);
        res.status(500).send({status: 'error', message: "Looks like there was a problem removing a rule."});
    });
}

const hideTransactions = async (transactionIds, hide, userId, res) => {
    try {
        for (let transactionId of transactionIds) {
            await Transaction.findOneAndUpdate({_id: transactionId, userId}, {$set: {hide}}).exec();
        }
    } catch(err) {
        console.log('Hide transaction error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem hiding a transaction.'});
    }
};

exports.hideTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await hideTransactions(req.body.transactionIds, req.body.hide, userId, res);
        return account(req, res);
    } catch(err) {
        console.log('Hide transactions error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem hiding transactions.'});
    }
}

const splitTransaction = async (transactionId, divisions, userId, budgetId, res) => {
    // * distribute the paycheck based on the budget profile
    try {
        let transaction = await Transaction.findOne({_id: transactionId, userId}).lean().exec();
        const divisionsSum = _.chain(divisions).values().map('amount').filter(_.isNumber).sum().value();
        if(divisionsSum > Math.abs(transaction.TRNAMT)) {
            // ! handle this somehow
            console.log('split division error: Division sum is greater than transaction amount');
            if(!res.headersSent) res.status(500).send({status: 'error', message: `Your divisions assumed a split amount of at least ${divisionsSum} but your transaction was only ${transaction.TRNAMT}.`});
        } else {
            let splitsFolder = await Folder.findOne({userId, name: 'Splits'}).lean().exec();
            await moveTransactions(userId, [transactionId], splitsFolder._id, budgetId, res);
            await Transaction.findOneAndUpdate({_id: transactionId, userId}, {$set: {type: 'split'}}).lean().exec();
            for(let [folderId, division] of Object.entries(divisions)) {
                const splitTransaction = Object.assign({}, transaction, {
                    type: 'split',
                    folderId,
                    TRNAMT: division.amount == 'remaining' ? transaction.TRNAMT + divisionsSum : -division.amount,
                    linkedTransactionId: transaction._id
                });

                delete splitTransaction._id;

                try {
                    await new Transaction(splitTransaction).save();
                } catch(err) {
                    console.log('transaction error:', err);
                    if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem saving the split transaction.'});
                }
            }
            await recalculateBudgets(userId, budgetId, res);
        }
    } catch(err) {
        console.log('split error:', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem splitting a transaction.'});
    }
};

exports.splitTransaction = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await splitTransaction(req.body.transactionId, req.body.divisions, userId, req.query.selectedBudgetId, res);
        return account(req, res);
    } catch(err) {
        console.log('Split transactions error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem hiding transactions.'});
    }
}

exports.mergeTransactions = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        const transactionsToRemove = await Transaction.find({linkedTransactionId: req.body.transactionId, userId}).lean().exec();
        await removeTransactions(_.map(transactionsToRemove, '_id'), req.query.selectedBudgetId, userId, res);
        await moveTransactions(userId, [req.body.transactionId], req.body.folderId, req.query.selectedBudgetId, res);
        await Transaction.findOneAndUpdate({_id: req.body.transactionId, userId}, {$set: {type: 'bank'}}).lean().exec();
        await recalculateBudgets(userId, req.query.selectedBudgetId, res);
        return account(req, res);
    } catch(err) {
        console.log('Merge transactions error:', err);
        res.status(500).send({status: 'error', message: 'Looks like there was a problem merging transactions.'});
    }
}

const transferAmount = async (userId, budgetId, fromFolderId, toFolderId, amount, res) => {
    let date = new Date();
    let todaysDate = parseInt("" + pad0(date.getFullYear()) + pad0(date.getMonth() + 1) + pad0(date.getDate()) + pad0(date.getHours()) + pad0(date.getMinutes()) + pad0(date.getSeconds()));
    try {
        let folders = await Folder.find({userId, _id: {$in: [fromFolderId, toFolderId]}}).lean().exec();
        folders = _.keyBy(folders, '_id');
        let toTransaction = await new Transaction({
            userId,
            bank: 'mrbacon',
            NAME: `Mr. Bacon Transfer: ${folders[fromFolderId].name}`,
            SIC: -1,
            FITID: `Mr. Bacon Transfer: ${folders[fromFolderId].name} ${todaysDate}`,
            TRNAMT: amount,
            DTPOSTED: todaysDate,
            TRNTYPE: null,
            folderId: toFolderId,
            budgetId,
            read: false,
            hide: false,
            posted: true,
            linkedTransactionId: null,
            type: 'transfer'
        }).save();
        let fromTransaction = await new Transaction({
            userId,
            bank: 'mrbacon',
            NAME: `Mr. Bacon Transfer: ${folders[toFolderId].name}`,
            SIC: -1,
            FITID: `Mr. Bacon Transfer: ${folders[toFolderId].name} ${todaysDate}`,
            TRNAMT: -amount,
            DTPOSTED: todaysDate,
            TRNTYPE: null,
            folderId: fromFolderId,
            budgetId,
            read: false,
            hide: false,
            posted: true,
            linkedTransactionId: toTransaction._id,
            type: 'transfer'
        }).save();
        await recalculateBudgets(userId, budgetId, res);
    } catch (err) {
        console.log('transfer error:', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem adding transfer transactions.'});
    }
}

exports.transferAmount = async (req, res) => {
    const userId = req.query.selectedUserId || req.user.id;

    try {
        await transferAmount(userId, req.query.selectedBudgetId, req.body.fromFolderId, req.body.toFolderId, req.body.amount, res);
        return account(req, res);
    } catch (err) {
        console.log('transfer error:', err);
        if(!res.headersSent) res.status(500).send({status: 'error', message: 'Looks like there was a problem transfering an amount.'});
    }
}

exports.setSelectedBudgetId = async (req, res, next) => {
    if (!req.query.selectedBudgetId && req.user) {
        const userId = req.query.selectedUserId || req.user.id;
        try {
            let latestBudget = await Budget.findOne({ userId }).sort({ date: -1 }).lean().exec();
            if (latestBudget) {
                req.query.selectedBudgetId = latestBudget._id;
                req.query.setSelectedBudgetId = true;
            }
            next();
        } catch (err) {
            console.log('selectedBudgetId error:', err);
            res.status(500).send({ status: 'error', message: "Looks like there was a problem getting the latest budget." });
        }
    } else {
        next();
    }
}
