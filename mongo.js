const Promise = require('bluebird');
const mongo = require('mongoose');

mongo.Promise = Promise;

exports.User = mongo.model('googleuser', {
    id: String,
    name: String,
    email: String,
    image: String
});
exports.Account = mongo.model('account', {
    userId: String,
    bank: String,
    key: String,
    balance: Number
});
exports.Transaction = mongo.model('transaction', {
    userId: String,
    bank: String,
    NAME: String,
    SIC: Number,
    FITID: String,
    TRNAMT: Number,
    DTPOSTED: Number,
    TRNTYPE: String,
    folderId: String,
    budgetId: String,
    read: Boolean,
    hide: Boolean,
    posted: Boolean,
    linkedTransactionId: String,
    type: String
});
exports.Folder = mongo.model('folder', {
    userId: String,
    name: String,
    amount: Number,
    permissions: {},
    order: Number,
    icon: String
});
exports.Rule = mongo.model('rule', {
    userId: String,
    name: String,
    folderId: String,
    parameters: [{}],
    isPaycheck: Boolean,
    budgetProfileId: String,
});
exports.BudgetProfile = mongo.model('budgetprofile', {
    userId: String,
    name: String,
    divisions: {},
});
exports.Budget = mongo.model('budget', {
    userId: String,
    date: Number,
    budgetProfileId: String,
    folderAmounts: {}
});
exports.Share = mongo.model('share', {
    userId: String,
    email: String
});

exports.mongo = mongo;