const Nightmare = require('nightmare');

Nightmare.Promise = Promise;
Nightmare.action('clickIf', function (selector, done) {
    this.evaluate_now(selector => {
        document.activeElement.blur();
        const element = document.querySelector(selector);
        if (element) {
            let event = document.createEvent('MouseEvent');
            event.initEvent('click', true, true);
            element.dispatchEvent(event);
        }
    }, done, selector);
});

module.exports = Nightmare;