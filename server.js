'use strict';

require('dotenv').load();
const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const Promise = require("bluebird");
const fs = require('fs');
const passport = require('passport');
const multer = require('multer');
const mongo = require('./mongo').mongo;

const upload = multer({dest: 'uploads/'});
const MongoStore = require('connect-mongo/es5')(session);
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
let conf = {
    db: {
        db: 'mrbacon',
        // host: 'localhost',
        // url: 'mongodb://localhost/mrbacon',
        url: process.env.NODE_ENV == 'production' || process.env.FORCE_MONGO_ATLAS_DB == 'true' ? `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_ATLAS_CLUSTER_URLS}/mrbacon?ssl=true&replicaSet=${process.env.MONGO_ATLAS_REPLICA_SET}&authSource=admin` : 'mongodb://localhost/mrbacon'
        // port: 6646,  // optional, default: 27017
        // username: 'admin', // optional
        // password: 'secret', // optional
        // collection: 'mySessions' // optional, default: sessions
    },
    secret: process.env.SESSION_SECRET
};
const api = require('./api.js');

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.  However, since this example does not
//   have a database of user records, the complete Google profile is
//   serialized and deserialized.

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: `${process.env.NODE_ENV == 'production' ? process.env.APPSPOT_URL : `http://localhost:${process.env.PORT}`}/auth/google/callback`
  },
  (accessToken, syncToken, profile, done) => {
    // asynchronous verification, for effect...
    // process.nextTick(function () {

      // To keep the example simple, the user's Google profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the Google account with a user record in your database,
      // and return that user instead.
      return done(null, profile);
    // });
  }
));

let app = express();

app.use(bodyParser.urlencoded({extended: false, limit: '10mb'/* , uploadDir:'./uploads' */}));
app.use(bodyParser.json({limit: '10mb'}));

app.use(cookieParser());
app.use(methodOverride());
app.use(session({
    secret: conf.secret,
    maxAge: new Date(Date.now() + 3600000),
    store: new MongoStore(conf.db),
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(api.setSelectedBudgetId);

const pathToApp = __dirname;
app.use('/static', express.static(__dirname + '/public'));

passport.serializeUser((user, done) => {
    done(null, user._json);
});

passport.deserializeUser((obj, done) => {
    done(null, obj);
});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'] }));

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/google/callback',  passport.authenticate('google', { failureRedirect: '/login' }), api.googleCallback);
app.get('/linkAccounts', api.ensureAuthenticated, (req, res) => {
    res.sendFile(pathToApp + '/index.html')
});
app.get('/logout', api.ensureAuthenticated, api.logout);
app.get('/account', api.ensureAuthenticated, (req, res) => {
    res.sendFile(pathToApp + '/index.html')
});
app.get('/', (req, res) => {
    res.sendFile(pathToApp + '/index.html')
});

app.get('/api/banks', api.supportedBanksEndpoint);

app.get('/api/account', api.ensureAuthenticated, api.account);
app.post('/api/account', api.ensureAuthenticated, api.sync);

app.post('/api/link/wellsfargo', api.ensureAuthenticated, upload.fields([{name: 'username'}, {name: 'password'}]), api.wellsfargo);
app.post('/api/link/usaa', api.ensureAuthenticated, upload.fields([{name: 'user'}, {name: 'password'}, {name: 'accId'}, {name: 'accType'}/* , {name: 'bankId'} */]), api.usaa);

app.post('/api/folder/transfer', api.ensureAuthenticated, api.transferAmount);
app.post('/api/folder', api.ensureAuthenticated, api.newFolder);
app.put('/api/folder/:id', api.ensureAuthenticated, api.editFolder);
app.delete('/api/folder/:id', api.ensureAuthenticated, api.removeFolder);

app.post('/api/rule', api.ensureAuthenticated, api.newRule);
app.put('/api/rule/:id', api.ensureAuthenticated, api.editRule);
app.delete('/api/rule/:id', api.ensureAuthenticated, api.removeRule);

app.post('/api/budgetProfile', api.ensureAuthenticated, api.newBudgetProfile);
app.put('/api/budgetProfile/:id', api.ensureAuthenticated, api.editBudgetProfile);
app.delete('/api/budgetProfile/:id', api.ensureAuthenticated, api.removeBudgetProfile);

app.post('/api/budget', api.ensureAuthenticated, api.newBudget);

app.get('/api/transaction', api.ensureAuthenticated, api.searchTransactions);
app.put('/api/transaction/split', api.ensureAuthenticated, api.splitTransaction);
app.put('/api/transaction/merge', api.ensureAuthenticated, api.mergeTransactions);
app.put('/api/transaction/hide', api.ensureAuthenticated, api.hideTransactions);
app.put('/api/transaction/folder', api.ensureAuthenticated, api.moveTransactions);
app.put('/api/transaction/budget', api.ensureAuthenticated, api.moveBudgetTransactions);
app.delete('/api/transaction', api.ensureAuthenticated, api.removeTransactions);

app.get('/api/data/fix', api.ensureAuthenticated, api.fixData);
app.get('/api/data', api.ensureAuthenticated, api.exportData);
app.put('/api/data', api.ensureAuthenticated, api.importData);

app.post('/api/share', api.ensureAuthenticated, api.newShare);
app.put('/api/share/:id', api.ensureAuthenticated, api.editShare);
app.delete('/api/share/:id', api.ensureAuthenticated, api.removeShare);

mongo.connect(conf.db.url, {promiseLibrary: Promise});
mongo.connection.on('open', () => {
    app.listen(process.env.PORT || 8080, () => {
        console.log(`express server listening on: ${process.env.PORT}`);
    });
});