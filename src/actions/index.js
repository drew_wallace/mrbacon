import 'whatwg-fetch';
import { isEmpty, map, sortBy, findIndex, filter, has, first, keys, values, keyBy } from 'lodash';
import download from '../lib/download';

export const toggleFoldersModal = (open) => {
    return (dispatch, getState) => {
        dispatch(fetchAccount(getState()));
        dispatch({
            type: 'FOLDERS_MODAL_OPEN',
            open
        });
    };
}

export const toggleRulesModal = (open) => {
    return (dispatch, getState) => {
        dispatch(fetchAccount(getState()));
        dispatch({
            type: 'RULES_MODAL_OPEN',
            open
        });
    };
}

export const toggleBudgetProfilesModal = (open) => {
    return (dispatch, getState) => {
        dispatch(fetchAccount(getState()));
        dispatch({
            type: 'BUDGET_PROFILES_MODAL_OPEN',
            open
        });
    };
}

export const toggleSyncModal = (open) => {
    return {
        type: 'SYNC_MODAL_OPEN',
        open
    };
}

export const toggleMoveModal = (open) => {
    return {
        type: 'MOVE_MODAL_OPEN',
        open
    };
}

export const toggleMoveBudgetModal = (open) => {
    return {
        type: 'MOVE_BUDGET_MODAL_OPEN',
        open
    };
}

export const invalidateBanks = (banks) => {
  return {
    type: 'INVALIDATE_BANKS',
    banks
  };
}

function requestBanks() {
    return {
        type: 'REQUEST_BANKS'
    };
}

function receiveBanks(banks) {
    return {
        type: 'RECEIVE_BANKS',
        banks,
        receivedAt: Date.now()
    };
}

function fetchBanks(state) {
    return dispatch => {
        dispatch(requestBanks());

        fetch('/api/banks').then(function(response) {
            return response.json();
        }).then(function(data) {
            let banks = data;
            dispatch(receiveBanks(banks));
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }
}

function shouldFetchBanks(state) {
    const banks = state.banks;

    if (isEmpty(banks)) {
        return true;
    } else if (banks.isFetching) {
        return false;
    } else {
        return banks.didInvalidate;
    }
}

export function fetchBanksIfNeeded() {

    // Note that the function also receives getState()
    // which lets you choose what to dispatch next.

    // This is useful for avoiding a network request if
    // a cached value is already available.

    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchBanks(state)) {
            // Dispatch a thunk from thunk!
            return dispatch(fetchBanks(state));
        } else {
            // Let the calling code know there's nothing to wait for.
            return Promise.resolve()
        }
    };
}

export const invalidateAccount = (account) => {
  return {
    type: 'INVALIDATE_ACCOUNT',
    account
  }
}

function requestAccount() {
    return {
        type: 'REQUEST_ACCOUNT'
    };
}

function requestSyncedAccount() {
    return {
        type: 'REQUEST_REFRESHED_ACCOUNT'
    };
}

function receiveAccount(account) {
    return {
        type: 'RECEIVE_ACCOUNT',
        account,
        receivedAt: Date.now()
    };
}

function fetchAccount(state) {
    return dispatch => {
        dispatch(requestAccount());

        fetch(`/api/account?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }
}

function shouldFetchAccount(state) {
    const account = state.account;

    if (isEmpty(account)) {
        return true;
    } else if (account.isFetching) {
        return false;
    } else {
        return account.didInvalidate;
    }
}

export function fetchAccountIfNeeded() {

    // Note that the function also receives getState()
    // which lets you choose what to dispatch next.

    // This is useful for avoiding a network request if
    // a cached value is already available.

    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchAccount(state)) {
            // Dispatch a thunk from thunk!
            return dispatch(fetchAccount(state));
        } else {
            // Let the calling code know there's nothing to wait for.
            return Promise.resolve()
        }
    };
}

export function changedSelectedFolder(id) {
    return {
        type: 'CHANGED_SELECTED_FOLDER',
        id
    };
}

export function syncDataClicked(encCredentials) {
    return (dispatch, getState) => {
        dispatch(requestSyncedAccount());
        dispatch(changedSelectedBudgetId(''));

        const state = getState();

        fetch(`/api/account?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'POST',
            body: JSON.stringify({encCredentials}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function createFolderClicked(folder) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/folder?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'POST',
            body: JSON.stringify(folder),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedFolder(folder) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/folder/${folder._id}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify(folder),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedBadFolderName(folderId, folderName) {
    return {
        type: 'EDITED_BAD_FOLDER_NAME',
        folderId,
        folderName
    }
}

export function editedBadFolderAmount(folderId, folderAmount) {
    return {
        type: 'EDITED_BAD_FOLDER_AMOUNT',
        folderId,
        folderAmount
    }
}

export function removeFolderClicked(folderId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/folder/${folderId}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'DELETE',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function createRuleClicked(rule) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/rule?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'POST',
            body: JSON.stringify(rule),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedRule(rule) {
    return (dispatch, getState) => {
        let state = getState();

        console.log(rule);

        fetch(`/api/rule/${rule._id}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify(rule),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedBadRuleName(ruleId, ruleName) {
    return {
        type: 'EDITED_BAD_RULE_NAME',
        ruleId,
        ruleName
    }
}

export function editedBadRuleParams(ruleId, ruleParams) {
    return {
        type: 'EDITED_BAD_RULE_PARAMS',
        ruleId,
        ruleParams
    }
}

export function removeRuleClicked(ruleId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/rule/${ruleId}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'DELETE',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function createBudgetProfileClicked(budgetProfile) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/budgetProfile?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'POST',
            body: JSON.stringify(budgetProfile),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedBudgetProfile(budgetProfile) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/budgetProfile/${budgetProfile._id}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify(budgetProfile),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedBadBudgetProfileName(budgetProfileId, budgetProfileName) {
    return {
        type: 'EDITED_BAD_BUDGET_PROFILE_NAME',
        budgetProfileId,
        budgetProfileName
    }
}

export function removeBudgetProfileClicked(budgetProfileId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/budgetProfile/${budgetProfileId}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'DELETE',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function createBudgetClicked() {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/budget?selectedTransactionId=${values(state.selectedTransactions)[0]._id}`, {
			method: 'POST',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function removedTransactions(transactionIds) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'DELETE',
            body: JSON.stringify({transactionIds}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function movedTransactions(transactionIds, folderId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction/folder?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify({transactionIds, folderId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function moveBudgetTransactions(transactionIds, budgetId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction/budget?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify({transactionIds, budgetId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function (ex) {
            console.error(ex)
        });
    };
}

export const changedSelectedTransactions = (transactions) => {
    return {
        type: 'CHANGED_SELECTED_TRANSACTIONS',
        transactions
    };
}

export const changedSelectedAllTransactions = (selectAll) => {
    return {
        type: 'CHANGED_SELECTED_ALL_TRANSACTIONS',
        selectAll
    };
}

export function changedSelectedBudgetId(id) {
    return {
        type: 'CHANGED_SELECTED_BUDGET',
        id
    };
}

export function changedSelectedBudget(budgetId) {
    return (dispatch, getState) => {
        let state = getState();
        const sortedBudgets = sortBy(map(state.account.data.budgets, budget => { return {_id: budget._id, date: budget.date}; }), budget => -budget.date);
        if(sortedBudgets[0]._id == budgetId) {
            budgetId = '';
        }

        dispatch(changedSelectedBudgetId(budgetId));

        fetch(`/api/account?selectedBudgetId=${budgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'GET',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function exportData() {
    return async (dispatch) => {
        try {
            const response = await fetch(`/api/data`, {
                method: 'GET',
                credentials: 'include'
            });
            let data = await response.json();
            download(JSON.stringify(data), 'mrbacon_data.txt', 'text/plain');
        } catch(err) {
            console.error(err)
        }
    };
}

export function fixData() {
    return async (dispatch) => {
        try {
            const response = await fetch(`/api/data/fix`, {
                method: 'GET',
                credentials: 'include'
            });
            let account = await response.json();
            dispatch(receiveAccount(account));
        } catch (err) {
            console.error(err)
        }
    };
}

export const toggleImportDataModal = (open) => {
    return {
        type: 'IMPORT_DATA_MODAL_OPEN',
        open
    };
}

export function importDataClicked(exportedData) {
    return (dispatch) => {
        dispatch(requestSyncedAccount());

        fetch('/api/data', {
			method: 'PUT',
            body: JSON.stringify({exportedData}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export const toggleSharesModal = (open) => {
    return (dispatch, getState) => {
        dispatch(fetchAccount(getState()));
        dispatch({
            type: 'SHARE_MODAL_OPEN',
            open
        });
    };
}

export function createShareClicked(share) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/share?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'POST',
            body: JSON.stringify(share),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedShare(share) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/share/${share._id}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify(share),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function editedBadShareEmail(shareId, shareEmail) {
    return {
        type: 'EDITED_BAD_SHARE_EMAIL',
        shareId,
        shareEmail
    }
}

export function removeShareClicked(shareId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/share/${shareId}?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
			method: 'DELETE',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

function changedSelectedUserId(id) {
    return {
        type: 'CHANGED_SELECTED_USER',
        id
    };
}

export function changedSelectedUser(userId) {
    return (dispatch, getState) => {
        let state = getState();
        dispatch(changedSelectedUserId(userId));

        fetch(`/api/account?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${userId}`, {
			method: 'GET',
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export function hideTransactions(transactionIds, hide) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction/hide?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify({transactionIds, hide}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
            let selectedFolderId = state.selectedFolderId || first(keys(account.folders));
            let updatedSelectedTransactions = keyBy(filter(account.folders[selectedFolderId].transactions, transaction => has(state.selectedTransactions, transaction._id)), '_id');
            dispatch(changedSelectedTransactions(updatedSelectedTransactions));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export const toggleSplitModal = (open) => {
    return (dispatch, getState) => {
        dispatch(fetchAccount(getState()));
        dispatch({
            type: 'SPLIT_MODAL_OPEN',
            open
        });
    };
}

export function splitTransaction(transactionId, divisions) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction/split?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify({transactionId, divisions}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export const toggleMergeModal = (open) => {
    return {
        type: 'MERGE_MODAL_OPEN',
        open
    };
}

export function mergedTransactions(transactionId, folderId) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/transaction/merge?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'PUT',
            body: JSON.stringify({transactionId, folderId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export const toggleTransferModal = (open) => {
    return {
        type: 'TRANSFER_MODAL_OPEN',
        open
    };
}

export function transferedAmount(fromFolderId, toFolderId, amount) {
    return (dispatch, getState) => {
        let state = getState();

        fetch(`/api/folder/transfer?selectedBudgetId=${state.selectedBudgetId}&selectedUserId=${state.selectedUserId}`, {
            method: 'POST',
            body: JSON.stringify({fromFolderId, toFolderId, amount}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            let account = data;
            dispatch(receiveAccount(account));
        }).catch(function(ex) {
            console.error(ex)
        });
    };
}

export const toggleStartingModal = (open) => {
    return {
        type: 'STARTING_MODAL_OPEN',
        open
    };
}