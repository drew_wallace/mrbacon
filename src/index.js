import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import { Router, Route, Link } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { MuiThemeProvider } from 'material-ui';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';

import Home from './views/containers/home';
import LinkAccounts from './views/containers/linkAccounts';
import Account from './views/containers/account';

import MrBaconApp from './reducers';

const customHistory = createBrowserHistory();
const initialState = {
    loggedIn: false,
    banks: {},
    account: {},
    selectedFolderId: '',
    foldersModalOpen: false,
    rulesModalOpen: false,
    budgetProfilesModalOpen: false,
    syncModalOpen: false,
    moveModalOpen: false,
    moveBudgetModalOpen: false,
    selectedTransactions: [],
    selectedAllTransactions: 0,
    selectedBudgetId: '',
    importDataModalOpen: false,
    sharesModalOpen: false,
    selectedUserId: '',
    splitModalOpen: false,
    mergeModalOpen: false,
    transferModalOpen: false,
    startingModalOpen: false
};
const loggerMiddleware = createLogger();
let store = createStore(MrBaconApp, initialState,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);

console.log(process.env.NODE_ENV);

ReactDOM.render(
    <Provider store={store}>
        <Router history={customHistory}>
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <Fragment>
                    <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/linkAccounts">Link Accounts</Link></li>
                        <li><Link to="/account">Account</Link></li>
                    </ul>

                    <hr/>

                    <Route exact path="/" component={Home}/>
                    <Route exact path="/linkAccounts" component={LinkAccounts}/>
                    <Route exact path="/account" component={Account}/>
                </Fragment>
            </MuiThemeProvider>
        </Router>
    </Provider>,
    document.getElementById('root')
);