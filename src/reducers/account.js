const account = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_ACCOUNT':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_REFRESHED_ACCOUNT':
        case 'REQUEST_ACCOUNT':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_ACCOUNT':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.account
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_FOLDER_NAME':
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    folders: Object.assign({}, state.data.folders, {
                        [action.folderId]: Object.assign({}, state.data.folders[action.folderId], {
                            name: action.folderName
                        })
                    })
                })
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_FOLDER_AMOUNT':
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    folders: Object.assign({}, state.data.folders, {
                        [action.folderId]: Object.assign({}, state.data.folders[action.folderId], {
                            amount: action.folderAmount
                        })
                    })
                })
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_RULE_NAME':
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    rules: Object.assign({}, state.data.rules, {
                        [action.ruleId]: Object.assign({}, state.data.rules[action.ruleId], {
                            name: action.ruleName
                        })
                    }),
                })
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_RULE_PARAMS':
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    rules: Object.assign({}, state.data.rules, {
                        [action.ruleId]: Object.assign({}, state.data.rules[action.ruleId], {
                            parameters: action.ruleParams
                        })
                    }),
                })
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_BUDGET_PROFILE_NAME':
            return Object.assign({}, state, {
                data: Object.assign({}, state.data, {
                    budgetProfiles: Object.assign({}, state.data.budgetProfiles, {
                        [action.budgetProfileId]: Object.assign({}, state.data.budgetProfiles[action.budgetProfileId], {
                            name: action.budgetProfileName
                        })
                    }),
                })
            });
        // ! Doesn't work if you save other fields. Maybe have an error field that doesn't get saved and overrides the default on the view
        case 'EDITED_BAD_SHARE_EMAIL':
        return Object.assign({}, state, {
            data: Object.assign({}, state.data, {
                shares: Object.assign({}, state.data.shares, {
                    [action.shareId]: Object.assign({}, state.data.shares[action.shareId], {
                        email: action.shareEmail
                    })
                }),
            })
        });
        default:
            return state
    }
}

export default account;