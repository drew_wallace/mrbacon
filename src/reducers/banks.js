const banks = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_BANKS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_BANKS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_BANKS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.banks
            });
        default:
            return state
    }
}

export default banks;