const budgetProfilesModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'BUDGET_PROFILES_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default budgetProfilesModalOpen;