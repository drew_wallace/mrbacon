const foldersModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'FOLDERS_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default foldersModalOpen;