const importDataModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'IMPORT_DATA_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default importDataModalOpen;