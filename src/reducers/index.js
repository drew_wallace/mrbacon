import { combineReducers } from 'redux';

import loggedIn from './loggedIn';
import banks from './banks';
import account from './account';
import selectedFolderId from './selectedFolderId';
import foldersModalOpen from './foldersModalOpen';
import rulesModalOpen from './rulesModalOpen';
import budgetProfilesModalOpen from './budgetProfilesModalOpen';
import syncModalOpen from './syncModalOpen';
import moveModalOpen from './moveModalOpen';
import moveBudgetModalOpen from './moveBudgetModalOpen';
import selectedTransactions from './selectedTransactions';
import selectedAllTransactions from './selectedAllTransactions';
import selectedBudgetId from './selectedBudgetId';
import importDataModalOpen from './importDataModalOpen';
import sharesModalOpen from './sharesModalOpen';
import selectedUserId from './selectedUserId';
import splitModalOpen from './splitModalOpen';
import mergeModalOpen from './mergeModalOpen';
import transferModalOpen from './transferModalOpen';
import startingModalOpen from './startingModalOpen';

const MrBaconApp = combineReducers({
    loggedIn,
    banks,
    account,
    selectedFolderId,
    foldersModalOpen,
    rulesModalOpen,
    budgetProfilesModalOpen,
    syncModalOpen,
    moveModalOpen,
    moveBudgetModalOpen,
    selectedTransactions,
    selectedAllTransactions,
    selectedBudgetId,
    importDataModalOpen,
    sharesModalOpen,
    selectedUserId,
    splitModalOpen,
    mergeModalOpen,
    transferModalOpen,
    startingModalOpen
});

export default MrBaconApp;