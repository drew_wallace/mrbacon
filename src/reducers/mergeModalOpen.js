const mergeModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'MERGE_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default mergeModalOpen;