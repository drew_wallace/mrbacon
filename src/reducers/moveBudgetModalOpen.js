const moveBudgetModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'MOVE_BUDGET_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default moveBudgetModalOpen;