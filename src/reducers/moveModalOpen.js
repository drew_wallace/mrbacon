const moveModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'MOVE_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default moveModalOpen;