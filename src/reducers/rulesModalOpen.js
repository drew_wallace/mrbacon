const rulesModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'RULES_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default rulesModalOpen;