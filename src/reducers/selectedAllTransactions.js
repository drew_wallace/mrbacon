const selectedAllTransactions = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGED_SELECTED_ALL_TRANSACTIONS':
            return action.selectAll;
        default:
            return state
    }
}

export default selectedAllTransactions;