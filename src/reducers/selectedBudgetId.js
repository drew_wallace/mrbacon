const selectedBudgetId = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGED_SELECTED_BUDGET':
            return action.id;
        default:
            return state
    }
}

export default selectedBudgetId;