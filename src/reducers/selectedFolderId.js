const selectedFolderId = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGED_SELECTED_FOLDER':
            return action.id;
        default:
            return state
    }
}

export default selectedFolderId;