const selectedTransactions = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGED_SELECTED_TRANSACTIONS':
            return {...action.transactions};
        default:
            return state
    }
}

export default selectedTransactions;