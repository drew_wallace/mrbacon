const selectedUserId = (state = {}, action) => {
    switch (action.type) {
        case 'CHANGED_SELECTED_USER':
            return action.id;
        default:
            return state
    }
}

export default selectedUserId;