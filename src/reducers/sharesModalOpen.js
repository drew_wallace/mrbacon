const sharesModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'SHARE_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default sharesModalOpen;