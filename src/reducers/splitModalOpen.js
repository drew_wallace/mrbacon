const splitModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'SPLIT_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default splitModalOpen;