const startingModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'STARTING_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default startingModalOpen;