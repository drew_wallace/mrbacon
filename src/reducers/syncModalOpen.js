const syncModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'SYNC_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default syncModalOpen;