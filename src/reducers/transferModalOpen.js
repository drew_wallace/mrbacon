const transferModalOpen = (state = {}, action) => {
    switch (action.type) {
        case 'TRANSFER_MODAL_OPEN':
            return action.open;
        default:
            return state
    }
}

export default transferModalOpen;