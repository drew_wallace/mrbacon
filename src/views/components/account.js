import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {map, find, countBy, pickBy, keys, first, get, isEmpty, has, sumBy, sum, filter, every, values, concat, round} from 'lodash';
import numeral from 'numeral';
import moment from 'moment';
import 'whatwg-fetch';

import download from '../../lib/download';
import debounce from '../../lib/debounce';

import { Link } from 'react-router-dom';
import { RaisedButton, FlatButton, MenuItem, Drawer, AppBar, IconButton, IconMenu, DropDownMenu, Dialog, Checkbox, Avatar, TextField } from 'material-ui';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import MoreVert from 'material-ui/svg-icons/navigation/more-vert';
import Sync from 'material-ui/svg-icons/notification/sync';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import IndeterminateCheckBox from 'material-ui/svg-icons/toggle/indeterminate-check-box';
import Delete from 'material-ui/svg-icons/action/delete';
import Folder from 'material-ui/svg-icons/file/folder';
import FlipToBack from 'material-ui/svg-icons/action/flip-to-back';
import FlipToFront from 'material-ui/svg-icons/action/flip-to-front';
import CallSplit from 'material-ui/svg-icons/communication/call-split';
import Person from 'material-ui/svg-icons/social/person';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import CallMerge from 'material-ui/svg-icons/communication/call-merge';
import Redo from 'material-ui/svg-icons/content/redo';
import PlayCircleOutline from 'material-ui/svg-icons/av/play-circle-outline';
import DateRange from 'material-ui/svg-icons/action/date-range';

import Pig from '../components/Pig';
import FoldersModal from '../containers/foldersModal';
import RulesModal from '../containers/rulesModal';
import BudgetProfilesModal from '../containers/budgetProfilesModal';
import SyncModal from '../containers/syncModal';
import MoveModal from '../containers/moveModal';
import MoveBudgetModal from '../containers/moveBudgetModal';
import ImportDataModal from '../containers/importDataModal';
import SharesModal from '../containers/sharesModal';
import SplitModal from '../containers/splitModal';
import MergeModal from '../containers/mergeModal';
import TransferModal from '../containers/transferModal';
import StartingModal from '../containers/startingModal';

export default class Account extends Component {
    constructor(props) {
        super(props);

		this.state = {
			sideMenuOpen: false,
			transactions: undefined
		}

		this.searchTransactions = debounce(this.searchTransactions, 300);
	}


    render() {
		let { account, selectedFolderId, fetchAccountIfNeeded, selectedTransactions, selectedAllTransactions, selectedBudgetId, selectedUserId } = this.props;

		if(!account.lastUpdated) {
			fetchAccountIfNeeded();
			return (<div></div>);
		}

		selectedFolderId = selectedFolderId || first(keys(account.data.folders));
		selectedBudgetId = selectedBudgetId || first(keys(account.data.budgets));

		const moveTransactions = selectedBudgetId && selectedAllTransactions != 0 && every(selectedTransactions, transaction => transaction.type == 'bank');
		const showTransactions = selectedBudgetId && selectedAllTransactions != 0 && every(selectedTransactions, transaction => transaction.hide);
		const mergeTransaction = selectedBudgetId && values(selectedTransactions).length == 1 && first(values(selectedTransactions)).type == 'split';
		const transferTransactions = selectedBudgetId && selectedAllTransactions != 0 && every(selectedTransactions, transaction => transaction.type == 'transfer' && transaction.linkedTransactionId);
		const startBudgetTransaction = values(selectedTransactions).length == 1;
		const showNoData = (this.state.transactions && this.state.transactions.length == 0) || (!this.state.transactions && get(account.data.folders[selectedFolderId], 'transactions', []).length == 0)

		return (
			<div>
				<Drawer open={this.state.sideMenuOpen} onRequestChange={(sideMenuOpen) => this.setState({sideMenuOpen})} docked={false}>
					<div style={{height: 200, padding: 10, backgroundColor: this.props.muiTheme.appBar.color, color: 'white'}}>
						<div style={{display: 'flex', justifyContent: 'center'}}>
							<Pig size={110}/>
						</div>
						<div style={{paddingBottom: 20, paddingTop: 10}}>
							{`${get(account, 'data.user.name.givenName')} ${get(account, 'data.user.name.familyName')}`}
						</div>
						<div style={{display: 'flex', justifyContent: 'space-between'}}>
							<div>
								<h3 className="text-center thin">{numeral(sumBy(account.data.accounts, 'balance')).format('$0,0.00')}</h3>
								<span>Balance</span>
							</div>
							<div>
								<h3 className="text-center thin">{numeral(round(sumBy(account.data.accounts, 'balance') - sum(map(account.data.folders, folder => folder.name != 'Splits' ? folder.amount : 0)), 2)).format('$0,0.00')}</h3>
								<span>Pending</span>
							</div>
						</div>
					</div>
					<MenuItem primaryText="Change Folders" onClick={() => this.handleFoldersClicked()}/>
					<MenuItem primaryText="Change Rules" onClick={() => this.handleRulesClicked()}/>
					<MenuItem primaryText="Change Budget Profiles" onClick={() => this.handleBudgetProfilesClicked()}/>
					<MenuItem primaryText="Import Data" onClick={() => this.handleImportDataClicked()}/>
					<MenuItem primaryText="Export Data" onClick={() => this.handleExportDataClicked()} />
					<MenuItem primaryText="Fix Data" onClick={() => this.handleFixDataClicked()}/>
				</Drawer>

				<AppBar
					title="Mr. Bacon"
					iconElementRight={
						<IconMenu
							iconButtonElement={
								<IconButton><MoreVert/></IconButton>
							}
							targetOrigin={{horizontal: 'right', vertical: 'top'}}
							anchorOrigin={{horizontal: 'right', vertical: 'top'}}
						>
							{selectedUserId == '' && <MenuItem primaryText="Sharing" onClick={() => this.handleShareClicked()}/>}
							<MenuItem
								primaryText="Shared Accounts"
								rightIcon={<ArrowDropRight/>}
								menuItems={[
									<MenuItem primaryText={'My Account'} checked={selectedUserId == ''} rightIcon={get(account, 'data.user.image.url') ? <Avatar src={get(account, 'data.user.image.url')}/> : <Person/>} onClick={() => this.handleUserClicked('')}/>,
									...map(filter(account.data.shares, share => share.email == get(find(account.data.user.emails, email => email.type == 'account'), 'value')), share =>
										<MenuItem primaryText={share.user.name} checked={selectedUserId == share.userId} rightIcon={share.user.image ? <Avatar src={share.user.image}/> : <Person/>} onClick={() => this.handleUserClicked(share.userId)}/>
									)
								]}
							/>
							<MenuItem primaryText="Logout" rightIcon={get(account, 'data.user.image.url') ? <Avatar src={get(account, 'data.user.image.url')}/> : <Person/>} onClick={() => location.href = '/logout'}/>
						</IconMenu>
					}
					onLeftIconButtonClick={() => this.setState({sideMenuOpen: true})}
				/>

				<div style={{display: 'flex', flexWrap: 'wrap'}}>
					<div style={{display: 'flex', flex: selectedBudgetId ? 0 : 1}}>
						<DropDownMenu value={selectedFolderId} listStyle={{minWidth: 250}} style={{margin: 0}} onChange={(e, key, value) => this.handleFolderClicked(value)} iconStyle={{fill: 'black'}} disabled={this.state.transactions}>
							{map(account.data.folders, folder => (
								<MenuItem
									value={folder._id}
									primaryText={folder.name}
									secondaryText={numeral(folder.amount).format('$0,0.00')}
									label={`${folder.name} ${numeral(folder.amount).format('$0,0.00')}`}
									rightIcon={<div className="material-icons">{folder.icon}</div>}
								/>
							))}
						</DropDownMenu>
					</div>
					{selectedBudgetId &&
						<div style={{display: 'flex', flex: 1}}>
							<DropDownMenu value={selectedBudgetId} style={{margin: 0}} onChange={(e, key, value) => this.handleBudgetClicked(value)} iconStyle={{fill: 'black'}} disabled={this.state.transactions}>
								{map(account.data.budgets, budget => (
									<MenuItem
										value={budget._id}
										primaryText={moment(budget.date, 'YYYYMMDDhhmmss').toDate().toDateString()}
										label={moment(budget.date, 'YYYYMMDDhhmmss').toDate().toDateString()}
									/>
								))}
							</DropDownMenu>
						</div>
					}
					{!selectedBudgetId &&
						<RaisedButton label="Get Started" primary={true} onClick={() => this.handleGetStartedClicked()} />
					}
					{selectedBudgetId &&
						<TextField id="search-box" placeholder="Search" onChange={(e, value) => this.searchTransactions(value)}/>
					}
					<div>
						{!selectedBudgetId && <IconButton tooltip="Start Budgeting" onClick={() => this.handleStartBudgetingClicked()} disabled={!startBudgetTransaction}><PlayCircleOutline /></IconButton>}
						<IconButton tooltip="Sync" onClick={() => this.handleSyncClicked()} disabled={this.state.transactions}><Sync/></IconButton>
						<IconButton tooltip="Transfer" onClick={() => this.handleTransferClicked()} disabled={isEmpty(account.data.budgets) || this.state.transactions}><Redo/></IconButton>
						{/* TODO: add a confirmation for remove */}
						<IconButton tooltip="Remove" onClick={() => this.handleRemovedTransactions()} disabled={!transferTransactions || this.state.transactions}><Delete/></IconButton>
						<IconButton tooltip="Move" onClick={() => this.handleMoveTransactions()} disabled={!moveTransactions || this.state.transactions}><Folder /></IconButton>
						<IconButton tooltip="Change Budget" onClick={() => this.handleMoveBudget()} disabled={!moveTransactions || this.state.transactions}><DateRange/></IconButton>
						{!selectedUserId && <IconButton tooltip={showTransactions ? 'Show' : 'Hide'} onClick={() => this.handleHideTransactions(showTransactions)} disabled={selectedAllTransactions == 0 || !selectedBudgetId || this.state.transactions}>{showTransactions ? <FlipToFront/> : <FlipToBack/>}</IconButton>}
						<IconButton tooltip={mergeTransaction ? 'Merge' : 'Split'} onClick={() => this.handleSplitTransactions(mergeTransaction)} disabled={keys(selectedTransactions).length != 1 || !selectedBudgetId || this.state.transactions}>{mergeTransaction ? <CallMerge/> : <CallSplit/>}</IconButton>
					</div>
				</div>

				<table className="responsive-table check-all">
					<thead>
						<tr>
							<td data-label="Check All">
								<Checkbox
									style={{width: 'auto'}}
									checked={selectedAllTransactions != 0}
									checkedIcon={selectedAllTransactions == 2 && <IndeterminateCheckBox />}
									onCheck={(e, value) => this.toggleSelectAll()}
									disabled={get(account.data.folders[selectedFolderId], 'transactions', []).length == 0 || this.state.transactions}
								/>
							</td>
							<td>Amount</td>
							<td>Source</td>
							<td>Name</td>
							<td>Date Posted</td>
						</tr>
					</thead>
					<tbody>
						{showNoData &&
							<tr><td colSpan="5">No Data</td></tr>
						}
						{(this.state.transactions || get(account.data.folders[selectedFolderId], 'transactions', [])).map(transaction =>
							<tr key={transaction._id}>
								<td data-label="Check">
									<Checkbox
										style={{width: 'auto'}}
										checked={!!selectedTransactions[transaction._id]}
										onCheck={(e, value) => this.toggleRow(transaction)}
										disabled={!(transaction.type == 'bank' || (transaction.type == 'transfer' && transaction.linkedTransactionId) || (transaction.type == 'split' && get(account.data.folders[selectedFolderId], 'name') == 'Splits')) || this.state.transactions}
									/>
								</td>
								<td data-label="Amount">
									{transaction.hide && selectedUserId ? 'Hidden' : numeral(transaction.TRNAMT).format('$0,0.00')}
								</td>
								<td data-label="Source">
									{this.getSourceCell(transaction)}
								</td>
								<td data-label="Name">
									{transaction.NAME}
								</td>
								<td data-label="Date Posted">
									{transaction.hide && selectedUserId ? 'Hidden' : moment(transaction.DTPOSTED, 'YYYYMMDDhhmmss').calendar()}
								</td>
							</tr>
						)}
					</tbody>
				</table>

				<FoldersModal/>
				<RulesModal/>
				<BudgetProfilesModal/>
				<SyncModal/>
				<MoveModal/>
				<MoveBudgetModal/>
				<ImportDataModal/>
				<SharesModal/>
				<SplitModal/>
				<MergeModal/>
				<TransferModal/>
				<StartingModal/>
			</div>
        );
	}

	toggleRow(transaction) {
		const transactionId = transaction._id;
		const selectedFolderId = this.props.selectedFolderId || first(keys(this.props.account.data.folders));
		let selectedAllTransactions = 2;
		const newSelected = Object.assign({}, this.props.selectedTransactions);

		if(has(this.props.selectedTransactions, transactionId)) {
			delete newSelected[transactionId];
		} else {
			newSelected[transactionId] = transaction;
		}

		let numberSelected = countBy(newSelected, item => item ? 1 : 0);
		if(!numberSelected[1]) {
			selectedAllTransactions = 0;
		} else if(has(this.props.account.data.folders[selectedFolderId], 'transactions') && numberSelected[1] == this.props.account.data.folders[selectedFolderId].transactions.filter(transaction => transaction.type == 'bank' || (transaction.type == 'split' && get(this.props.account.data.folders[selectedFolderId], 'name') == 'Splits')).length) {
			selectedAllTransactions = 1;
		}

		this.props.changedSelectedTransactions(newSelected);
		this.props.changedSelectedAllTransactions(selectedAllTransactions);
	}

	toggleSelectAll() {
		const selectedFolderId = this.props.selectedFolderId || first(keys(this.props.account.data.folders));
		let newSelected = {};

		if (this.props.selectedAllTransactions == 0) {
			this.props.account.data.folders[selectedFolderId].transactions.forEach(transaction => {
				if(transaction.type == 'bank' || (transaction.type == 'split' && get(this.props.account.data.folders[selectedFolderId], 'name') == 'Splits')) {
					newSelected[transaction._id] = transaction;
				}
			});
		}

		console.log(newSelected);

		this.props.changedSelectedTransactions(newSelected);
		this.props.changedSelectedAllTransactions(this.props.selectedAllTransactions == 0 && values(newSelected).length > 0 ? 1 : 0);
	}

	getSourceCell(transaction) {
		return <Avatar src={get(this.props.account.data, `banks.${transaction.bank}.thumbnail`, '')}/>
	}

	handleFolderClicked(id) {
		this.props.changedSelectedTransactions([]);
		this.props.changedSelectedAllTransactions(0);
		this.props.changedSelectedFolder(id);
	}

	handleFoldersClicked() {
		this.props.toggleFoldersModal(true);
		this.setState({sideMenuOpen: false});
	}

	handleRulesClicked() {
		this.props.toggleRulesModal(true);
		this.setState({sideMenuOpen: false});
	}

	handleBudgetProfilesClicked() {
		this.props.toggleBudgetProfilesModal(true);
		this.setState({sideMenuOpen: false});
	}

	handleSyncClicked() {
		this.props.toggleSyncModal(true);
	}

	handleRemovedTransactions() {
		let transactions = pickBy(this.props.selectedTransactions, item => item);
		this.props.removedTransactions(concat(keys(transactions), map(filter(values(transactions), item => item.linkedTransactionId), item => item.linkedTransactionId)));
		this.props.changedSelectedTransactions([]);
		this.props.changedSelectedAllTransactions(0);
	}

	handleMoveTransactions() {
		this.props.toggleMoveModal(true);
	}

	handleMoveBudget() {
		this.props.toggleMoveBudgetModal(true);
	}

	handleBudgetClicked(id) {
		this.props.changedSelectedTransactions([]);
		this.props.changedSelectedAllTransactions(0);
		this.props.changedSelectedBudget(id);
	}

	handleImportDataClicked(id) {
		this.props.toggleImportDataModal(true);
		this.setState({sideMenuOpen: false});
	}

	handleExportDataClicked() {
		this.props.exportData();
		this.setState({sideMenuOpen: false});
	}

	handleFixDataClicked() {
		this.props.fixData();
		this.setState({sideMenuOpen: false});
	}

	handleShareClicked() {
		this.props.toggleSharesModal(true);
	}

	handleUserClicked(id) {
		this.props.changedSelectedFolder('');
		this.props.changedSelectedBudgetId('');
		this.props.changedSelectedTransactions([]);
		this.props.changedSelectedAllTransactions(0);
		this.props.changedSelectedUser(id);
	}

	handleHideTransactions(show) {
		this.props.hideTransactions(keys(this.props.selectedTransactions), !show);
	}

	handleSplitTransactions(merge) {
		if(merge) {
			this.props.toggleMergeModal(true);
		} else {
			this.props.toggleSplitModal(true);
		}
	}

	handleTransferClicked() {
		this.props.toggleTransferModal(true);
	}

	handleGetStartedClicked() {
		this.props.toggleStartingModal(true);
	}

	handleStartBudgetingClicked() {
		this.props.createBudgetClicked();
		this.props.changedSelectedTransactions([]);
		this.props.changedSelectedAllTransactions(0);
	}

	searchTransactions(transactionSearch) {
		if (transactionSearch) {
			fetch(`/api/transaction?search=${transactionSearch}&selectedBudgetId=${this.props.selectedBudgetId}&selectedUserId=${this.props.selectedUserId}`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json'
				},
				credentials: 'include'
			}).then(response => {
				return response.json();
			}).then(data => {
				this.setState({transactions: data});
			}).catch(ex => {
				console.error(ex)
			});
		} else {
			this.setState({transactions: undefined})
		}
	}
};
