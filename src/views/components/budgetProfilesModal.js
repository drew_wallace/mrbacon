import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, first, values, pickBy, get, set } from 'lodash';
import { Dialog, FlatButton, RaisedButton , IconButton, DropDownMenu, MenuItem, Checkbox, Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class BudgetProfilesModal extends Component {
    constructor(props) {
		super(props);

		this.state = {
			selectedBudgetProfileId: 'new',
			newBudgetProfileName: '',
			newBudgetProfileDivisions: {}
		}
    }

    render() {
		let { account, budgetProfilesModalOpen, toggleBudgetProfilesModal, removeBudgetProfileClicked } = this.props;

		let isNewBudgetProfile = this.state.selectedBudgetProfileId === "new";
		let budgetProfile = account.budgetProfiles[this.state.selectedBudgetProfileId];

		let iconButtonTooltip = 'Create';
		let iconButtonAction = this.createBudgetProfile;
		let iconButtonIcon = <ContentAdd/>;
		let iconButtonDisabled = find(account.budgetProfiles, budgetProfile => budgetProfile.name == this.state.newBudgetProfileName) || !this.state.newBudgetProfileName || values(this.state.newBudgetProfileDivisions).length == 0;

		let textFieldValue = this.state.newBudgetProfileName;
		let textFieldAction = (e, value) => this.setState({newBudgetProfileName: value});

		if(!isNewBudgetProfile) {
			iconButtonTooltip = 'Delete';
			iconButtonAction = this.removeBudgetProfile;
			iconButtonIcon = <ContentRemove/>;
			iconButtonDisabled = false;

			textFieldValue = budgetProfile.name;
			textFieldAction = (e, value) => this.editBudgetProfileName(value);
		}

        return (
			<Dialog
				title="Change Budget Profiles"
				actions={[
					<FlatButton
						label="Done"
						primary={true}
						onClick={() => {
							toggleBudgetProfilesModal(false);
							this.setState({
								selectedBudgetProfileId: 'new',
								newBudgetProfileName: '',
								newBudgetProfileDivisions: {}
							});
						}}
					/>,
				]}
				modal={true}
				open={budgetProfilesModalOpen}
				autoScrollBodyContent={true}
				contentStyle={{width: '100%', height: '100%'}}
			>
				<div style={{display: 'flex', alignItems: 'center'}}>
					<DropDownMenu value={this.state.selectedBudgetProfileId} onChange={(e, key, value) => this.setState({selectedBudgetProfileId: value})} iconStyle={{fill: 'black'}}>
						<MenuItem
							value="new"
							primaryText="New"
						/>
						{map(account.budgetProfiles, budgetProfile => (
							<MenuItem
								key={budgetProfile._id}
								value={budgetProfile._id}
								primaryText={budgetProfile.name}
							/>
						))}
					</DropDownMenu>
					<IconButton tooltip={iconButtonTooltip} onClick={() => iconButtonAction()} disabled={iconButtonDisabled}>
						{iconButtonIcon}
					</IconButton>
					<TextField floatingLabelText="Budget Profile Name" value={textFieldValue} onChange={textFieldAction} />
				</div>
				<table className="responsive-table hide-all">
					<thead>
						<tr>
							<td>Enabled</td>
							<td>Folder</td>
							<td>Amount</td>
							<td>Use Remaining</td>
							<td>Transfer</td>
						</tr>
					</thead>
					<tbody>
						{map(account.folders, folder => {
							let enabledCheckboxChecked = has(this.state.newBudgetProfileDivisions, folder._id);
							let enabledCheckboxAction = this.newBudgetProfileToggleDivisions.bind(this);

							let textFieldAmountValue = get(this.state.newBudgetProfileDivisions[folder._id], 'amount') == 'remaining' ? '' : get(this.state.newBudgetProfileDivisions[folder._id], 'amount') || '';
							let textFieldAmountAction = this.newBudgetProfileDivisionsAmount.bind(this);

							let remainingCheckboxChecked = get(this.state.newBudgetProfileDivisions[folder._id], 'amount') == 'remaining';
							let remainingCheckboxAction = this.newBudgetProfileDivisionsAmount.bind(this);

							let transferCheckboxChecked = get(this.state.newBudgetProfileDivisions[folder._id], 'transfer', false);
							let transferCheckboxAction = this.newBudgetProfileDivisionsTransfer.bind(this);
							let transferCheckboxDisabled = !has(this.state.newBudgetProfileDivisions, folder._id) || folder.name == 'Savings';

							if (!isNewBudgetProfile) {
								enabledCheckboxChecked = has(budgetProfile.divisions, folder._id);
								enabledCheckboxAction = this.editBudgetProfileToggleDivisions.bind(this);

								textFieldAmountValue = get(budgetProfile.divisions[folder._id], 'amount') == 'remaining' ? '' : get(budgetProfile.divisions[folder._id], 'amount') || '';
								textFieldAmountAction = this.editBudgetProfileDivisionsAmount.bind(this);

								remainingCheckboxChecked = get(budgetProfile.divisions[folder._id], 'amount') == 'remaining';
								remainingCheckboxAction = this.editBudgetProfileDivisionsAmount.bind(this);

								transferCheckboxChecked = get(budgetProfile.divisions[folder._id], 'transfer', false);
								transferCheckboxAction = this.editBudgetProfileDivisionsTransfer.bind(this);
								transferCheckboxDisabled = !has(budgetProfile.divisions, folder._id) || folder.name == 'Savings';
							}

							if (!folder.permissions.budgetProfile) return null;
							return (
								<tr key={folder._id}>
									<td data-label="Enabled">
										<Checkbox style={{width: 'auto'}} checked={enabledCheckboxChecked} onCheck={(e, isChecked) => enabledCheckboxAction(folder._id, isChecked)} />
									</td>
									<td data-label="Folder">
										{/* TODO: include folder icon */}
										{folder.name}
									</td>
									<td data-label="Amount">
										<TextField
											id={folder._id}
											value={textFieldAmountValue}
											onChange={(e, value) => textFieldAmountAction(folder._id, value)}
										/>
									</td>
									<td data-label="Use Remaining">
										<Checkbox style={{width: 'auto'}} checked={remainingCheckboxChecked} onCheck={(e, isChecked) => remainingCheckboxAction(folder._id, isChecked ? 'remaining' : '')} />
									</td>
									<td data-label="Transfer">
										<Checkbox style={{width: 'auto'}} checked={transferCheckboxChecked} onCheck={(e, isChecked) => transferCheckboxAction(folder._id, isChecked)} disabled={transferCheckboxDisabled} />
									</td>
								</tr>
							)
						})}
					</tbody>
				</table>
			</Dialog>
        );
	}

	createBudgetProfile() {
		this.props.createBudgetProfileClicked({
			name: this.state.newBudgetProfileName,
			divisions: this.state.newBudgetProfileDivisions,
		});

		this.setState({
			selectedBudgetProfileId: 'new',
			newBudgetProfileName: '',
			newBudgetProfileDivisions: {}
		});
	}

	newBudgetProfileToggleDivisions(folderId, isEnabled) {
		let newBudgetProfileDivisions = this.state.newBudgetProfileDivisions;

		if(isEnabled) {
			newBudgetProfileDivisions[folderId] = '';
		} else {
			delete newBudgetProfileDivisions[folderId];
		}

		this.setState({newBudgetProfileDivisions});
	}

	newBudgetProfileDivisionsAmount(folderId, amount) {
		let newBudgetProfileDivisions = this.state.newBudgetProfileDivisions;

		if(amount) {
			if(amount == 'remaining') {
				newBudgetProfileDivisions = pickBy(newBudgetProfileDivisions, division => division.amount != 'remaining');
				set(newBudgetProfileDivisions, `${folderId}.amount`, amount);
			} else {
				set(newBudgetProfileDivisions, `${folderId}.amount`, Number(amount));
			}
		} else {
			delete newBudgetProfileDivisions[folderId];
		}

		this.setState({newBudgetProfileDivisions});
	}

	newBudgetProfileDivisionsTransfer(folderId, isTransfer) {
		let newBudgetProfileDivisions = this.state.newBudgetProfileDivisions;

		if(has(newBudgetProfileDivisions, folderId)) {
			set(newBudgetProfileDivisions, `${folderId}.transfer`, isTransfer);
		}

		this.setState({newBudgetProfileDivisions});
	}

	editBudgetProfileName(name) {
		if(!name) {
			this.props.editedBadBudgetProfileName(this.state.selectedBudgetProfileId, name);
		} else {
			this.props.editedBudgetProfile({...this.props.account.budgetProfiles[this.state.selectedBudgetProfileId], name});
		}
	}

	editBudgetProfileToggleDivisions(folderId, isEnabled) {
		let divisions = this.props.account.budgetProfiles[this.state.selectedBudgetProfileId].divisions;

		if(isEnabled) {
			divisions[folderId] = '';
		} else {
			delete divisions[folderId];
		}

		this.props.editedBudgetProfile({...this.props.account.budgetProfiles[this.state.selectedBudgetProfileId], divisions});
	}

	editBudgetProfileDivisionsAmount(folderId, amount) {
		let divisions = this.props.account.budgetProfiles[this.state.selectedBudgetProfileId].divisions;

		if(amount) {
			if(amount == 'remaining') {
				divisions = pickBy(divisions, division => division.amount != 'remaining');
				set(divisions, `${folderId}.amount`, amount);
			} else {
				set(divisions, `${folderId}.amount`, Number(amount));
			}
		} else {
			delete divisions[folderId];
		}

		this.props.editedBudgetProfile({...this.props.account.budgetProfiles[this.state.selectedBudgetProfileId], divisions});
	}

	editBudgetProfileDivisionsTransfer(folderId, isTransfer) {
		let divisions = this.props.account.budgetProfiles[this.state.selectedBudgetProfileId].divisions;

		if(has(divisions, folderId)) {
			set(divisions, `${folderId}.transfer`, isTransfer);
		}

		this.props.editedBudgetProfile({...this.props.account.budgetProfiles[this.state.selectedBudgetProfileId], divisions});
	}

	removeBudgetProfile() {
		this.props.removeBudgetProfileClicked(this.state.selectedBudgetProfileId);
		this.setState({selectedBudgetProfileId: 'new'});
	}
};
