import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, isEmpty, find } from 'lodash';
import { Dialog, FlatButton, IconButton } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class FoldersModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			newFolderName: '',
			newFolderAmount: '0'
		}
    }

    render() {
		let { account, foldersModalOpen, toggleFoldersModal, removeFolderClicked } = this.props;

        return (
			<Dialog
				title="Change Folders"
				actions={[
					<FlatButton
						label="Done"
						primary={true}
						onClick={() => {
							toggleFoldersModal(false);
						}}
					/>,
				]}
				modal={true}
				open={foldersModalOpen}
				autoScrollBodyContent={true}
			>
			<IconButton tooltip="Create" onClick={() => this.createFolder()} disabled={find(account.folders, folder => folder.name == this.state.newFolderName) || !this.state.newFolderName || !this.state.newFolderAmount}>
				<ContentAdd />
			</IconButton>
			<TextField floatingLabelText="Folder Name" value={this.state.newFolderName} onChange={(e, value) => this.setState({newFolderName: value})} autoFocus/>
			{isEmpty(account.budgets) && <TextField floatingLabelText="Starting Amount" value={this.state.newFolderAmount} onChange={(e, value) => this.setState({newFolderAmount: value})}/>}
			{map(account.folders, (folder, _id) => {
				if(!folder.permissions.change) return null;
				return (
					<div key={_id}>
						<IconButton tooltip="Delete" onClick={() => removeFolderClicked(_id)}>
							<ContentRemove />
						</IconButton>
						<TextField floatingLabelText="Folder Name" value={folder.name} onChange={(e, value) => this.editFolderName(_id, value)}/>
						{isEmpty(account.budgets) && <TextField floatingLabelText="Starting Amount" value={folder.amount} onChange={(e, value) => this.editFolderAmount(_id, value)}/>}
					</div>
				);
			})}
			</Dialog>
        );
	}

	createFolder() {
		this.props.createFolderClicked({
			name: this.state.newFolderName,
			amount: this.state.newFolderAmount,
			order: Object.keys(this.props.account.folders).length,
			icon: null
		});
		this.setState({
			newFolderName: '',
			newFolderAmount: '0'
		});
	}

	editFolderName(folderId, name) {
		if(!name) {
			this.props.editedBadFolderName(folderId, name);
		} else {
			this.props.editedFolder({...this.props.account.folders[folderId], name});
		}
	}

	editFolderAmount(folderId, amount) {
		if(amount == '') {
			this.props.editedBadFolderAmount(folderId, amount);
		} else {
			amount = Number(amount);
			this.props.editedFolder({...this.props.account.folders[folderId], amount});
		}
	}
};
