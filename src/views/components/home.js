import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { FlatButton } from 'material-ui';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {loggedIn} = this.props;

        return (
			<div>
				<h1 className="text-center">Mr. Bacon</h1>
				<h3>This is a budget app. It's simple. Link your accounts and start budgeting. Enjoy.</h3>
				{!loggedIn && (
					<FlatButton label="Sign in with Google" onClick={() => {location = "/auth/google";}}/>
				)}
			</div>
        );
    }
};

export default Home;