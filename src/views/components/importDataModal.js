import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has } from 'lodash';
import { Dialog, FlatButton, IconButton, RaisedButton } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class ImportDataModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			uploadedFileName: '',
			uploadedFileContents: '',
		}
    }

    render() {
		let { importDataModalOpen, toggleImportDataModal, importDataClicked } = this.props;

        return (
			<Dialog
				title="Import Data"
				actions={[
					<FlatButton
						label="Close"
						primary={true}
						onClick={() => {
							this.refs.importDataForm.reset();
							toggleImportDataModal(false);
							this.setState({uploadedFileContents: '', uploadedFileName: ''});
						}}
					/>,
					<FlatButton
						label="Submit"
						primary={true}
						disabled={!this.state.uploadedFileContents}
						onClick={() => {
							importDataClicked(this.state.uploadedFileContents);
							this.refs.importDataForm.reset();
							toggleImportDataModal(false);
							this.setState({uploadedFileContents: '', uploadedFileName: ''});
						}}
					/>,
				]}
				modal={true}
				open={importDataModalOpen}
			>
				<form ref="importDataForm">
					<RaisedButton
						containerElement="label"
						label="Upload exported data file"
					>
						<input
							type="file"
							onChange={changeEvent => {
								const file = changeEvent.target.files[0];

								if (file) {
									let fileReader = new FileReader();

									fileReader.onload = loadEvent => {
										const contents = loadEvent.target.result;
										this.setState({
											uploadedFileName: file.name,
											uploadedFileContents: JSON.parse(contents)
										})
									}

									fileReader.readAsText(file);
								} else {
									console.error("Failed to load file");
								}
							}}
							style={{
								width: 0.1,
								height: 0.1,
								opacity: 0,
								overflow: 'hidden',
								position: 'absolute',
								zIndex: -1,
							}}
						/>
					</RaisedButton>
					<span style={{paddingLeft: 10}}>{this.state.uploadedFileName}</span>
				</form>
			</Dialog>
        );
	}
};
