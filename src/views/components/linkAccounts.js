import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { RaisedButton, TextField, DropDownMenu, MenuItem } from 'material-ui';
import download from '../../lib/download';
import 'whatwg-fetch';

export default class LinkAccounts extends Component {
    constructor(props) {
        super(props);

		this.state = {
			search: '',
			form: '',
			credentials: []
		};
    }


    render() {
        let { banks, fetchBanksIfNeeded } = this.props;

		if(!banks.lastUpdated) {
			fetchBanksIfNeeded();
			return (<div></div>);
		}

        return (
			<div>
				<h1>Link Accounts</h1>
				<h4>Here's how this works: Sign in to each bank you want to link. Each bank you link, I'll send you a file with a key in it. When I need to login to your bank to get your transactions, I'll ask for that key. Keep it somewhere secure and accessible. That's the only copy of that key that exists. If you lose it, you'll have to re-link your account to get a new key.</h4>
				<div style={{display: 'flex', flexDirection: 'column'}}>
					<div>
						<TextField type="text" onChange={(e, value) => this.setState({search: value})} floatingLabelText="Find your bank"/>
						<RaisedButton onClick={() => this.doneLinkingAccounts()} label="Done" primary={true} style={{display: (this.state.credentials.length > 0 ? 'inline-block' : 'none')}}/>
						<div>
							{banks.data.filter(bank => bank.form).map(bank => (
								<form
									style={{display: (this.state.form === bank.formId ? 'block' : 'none')}}
									ref={bank.formId}
								>
									<h3>Login to {bank.name}</h3>
									{bank.form.map(field => {
										if(field.type === 'text' || field.type === 'password') {
											return (
												<TextField type={field.type} name={field.name} autoFocus={field.autoFocus} floatingLabelText={field.placeholder}/>
											);
										} else if(field.type === 'select') {
											// TODO: This is now controlled and will need to have an onChange handler once I have more than 1 option.
											return (
												<div>
													<DropDownMenu value={field.defaultValue}>
														{field.options.map(option => (<MenuItem value={option.value} primaryText={option.label} />))}
													</DropDownMenu>
													<input type="hidden" name={field.name} value={field.defaultValue}/>
												</div>
											);
										}
									})}
									<RaisedButton primary={true} label="Link" onClick={() => this.processForm(bank.formId, bank.route)}/>
								</form>
							))}
						</div>
					</div>
					<div style={{display: 'flex'}}>
						{banks.data.filter(bank => bank.form).map(bank => (
							<div style={{flex: '0 0 25%'}}>
								<img
									style={{width: 150, height: 150, position: 'relative', display: (bank.name.toLowerCase().indexOf(this.state.search) != -1 ? 'block' : 'none')}}
									alt={bank.name}
									src={bank.thumbnail}
									target={bank.formId}
									onClick={() => this.setState({form: (bank.formId !== this.state.form ? bank.formId : '')})}
								/>
							</div>
						))}
					</div>
				</div>

				<iframe style={{display: 'none'}} name="post_target"></iframe>
			</div>
        );
    }

	processForm(formId, route) {
		let form = this.refs[formId];

		fetch(route, {
			method: 'POST',
			body: new FormData(form),
			credentials: 'include'
		}).then((response) => response.json()).then((data) => {
			this.state.credentials.push(data);
            this.setState({credentials: this.state.credentials});
        }).catch(function(ex) {
            console.error('parsing failed', ex)
        });
	}

	doneLinkingAccounts() {
		download(JSON.stringify(this.state.credentials), 'mrbacon_credentials.txt', 'text/plain');
		this.props.history.push('/account');
	}
};
