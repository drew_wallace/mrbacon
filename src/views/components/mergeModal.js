import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, mapValues, keys, pickBy } from 'lodash';
import numeral from 'numeral';
import { Dialog, FlatButton, IconButton, DropDownMenu, MenuItem } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class MergeModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			selectedFolderId: '',
		}
    }

    render() {
		let { account, mergeModalOpen, toggleMergeModal, mergedTransactions, selectedTransactions, changedSelectedTransactions, changedSelectedAllTransactions } = this.props;

        return (
			<Dialog
				title="Merge Transactions"
				actions={[
					<FlatButton
						label="Close"
						primary={true}
						onClick={() => {
							toggleMergeModal(false);
							this.setState({selectedFolderId: ''});
						}}
					/>,
					<FlatButton
						label="Submit"
						primary={true}
						disabled={!this.state.selectedFolderId}
						onClick={() => {
							mergedTransactions(keys(pickBy(selectedTransactions, item => item)), this.state.selectedFolderId);
							toggleMergeModal(false);
							changedSelectedTransactions([]);
							changedSelectedAllTransactions(0);
							this.setState({selectedFolderId: ''});
						}}
					/>,
				]}
				modal={true}
				open={mergeModalOpen}
			>
				<DropDownMenu value={this.state.selectedFolderId} onChange={(e, key, value) => this.setState({selectedFolderId: value})} iconStyle={{fill: 'black'}} listStyle={{minWidth: 250}}>
					<MenuItem
						value=""
						primaryText="Select a folder"
					/>
					{map(account.data.folders, folder => (
						folder.permissions.move && <MenuItem
							value={folder._id}
							primaryText={folder.name}
							secondaryText={numeral(folder.amount).format('$0,0.00')}
							label={`${folder.name} ${numeral(folder.amount).format('$0,0.00')}`}
							rightIcon={<div className="material-icons">{folder.icon}</div>}
						/>
					))}
				</DropDownMenu>
			</Dialog>
        );
	}
};
