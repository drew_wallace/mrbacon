import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, mapValues, keys, pickBy } from 'lodash';
import numeral from 'numeral';
import moment from 'moment';
import { Dialog, FlatButton, IconButton, DropDownMenu, MenuItem } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class MoveBudgetModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			selectedBudgetId: '',
		}
    }

    render() {
		let { account, moveBudgetModalOpen, toggleMoveBudgetModal, moveBudgetTransactions, selectedTransactions, changedSelectedTransactions, changedSelectedAllTransactions } = this.props;

        return (
			<Dialog
				title="Move Transactions to Budget"
				actions={[
					<FlatButton
						label="Close"
						primary={true}
						onClick={() => {
							toggleMoveBudgetModal(false);
							{/* changedSelectedTransactions([]);
							changedSelectedAllTransactions(0); */}
							this.setState({selectedBudgetId: ''});
						}}
					/>,
					<FlatButton
						label="Submit"
						primary={true}
						disabled={!this.state.selectedBudgetId}
						onClick={() => {
							moveBudgetTransactions(keys(pickBy(selectedTransactions, item => item)), this.state.selectedBudgetId);
							toggleMoveBudgetModal(false);
							changedSelectedTransactions([]);
							changedSelectedAllTransactions(0);
							this.setState({selectedBudgetId: ''});
						}}
					/>,
				]}
				modal={true}
				open={moveBudgetModalOpen}
			>
				<DropDownMenu value={this.state.selectedBudgetId} onChange={(e, key, value) => this.setState({selectedBudgetId: value})} iconStyle={{fill: 'black'}}>
					<MenuItem
						value=""
						primaryText="Select a budget"
					/>
					{map(account.data.budgets, budget => (
						<MenuItem
							value={budget._id}
							primaryText={moment(budget.date, 'YYYYMMDDhhmmss').toDate().toDateString()}
							label={moment(budget.date, 'YYYYMMDDhhmmss').toDate().toDateString()}
						/>
					))}
				</DropDownMenu>
			</Dialog>
        );
	}
};
