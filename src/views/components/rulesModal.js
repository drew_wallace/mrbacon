import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, first, values, filter, find, sortBy, get } from 'lodash';
import { Dialog, FlatButton, RaisedButton , IconButton, DropDownMenu, MenuItem, Checkbox } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class RulesModal extends Component {
    constructor(props) {
		super(props);

		this.state = {
			selectedRuleId: 'new',
			newRuleName: '',
			newRuleFolderId: 'new',
			newRuleParams: [],
			newRuleIsPaycheck: false,
			newRuleBudgetProfileId: 'new'
		}
	}

	// TODO: also, I need to fix the done button not adding a rule if the rule is filled out...
	// TODO: make a 'Move transaction now' button that moves all transactions in the current folder to the new one

    render() {
		let { account, rulesModalOpen, toggleRulesModal, removeRuleClicked } = this.props;
		let dialogBody = (
			<div>
					<div style={{display: 'flex'}}>
						<div style={{flex: 1}}>
							<IconButton tooltip="Create" onClick={() => this.createRule()} disabled={find(account.rules, rule => rule.name == this.state.newRuleName) || !this.state.newRuleName || (!this.state.newRuleIsPaycheck && this.state.newRuleFolderId == 'new') || this.state.newRuleParams.length == 0 || (this.state.newRuleIsPaycheck && this.state.newRuleBudgetProfileId == 'new')}>
								<ContentAdd />
							</IconButton>
							<TextField floatingLabelText="Rule Name" value={this.state.newRuleName} onChange={(e, value) => this.setState({newRuleName: value})}/>
						</div>
						<Checkbox label='Is Paycheck' checked={this.state.newRuleIsPaycheck} onCheck={(e, checked) => this.setState({newRuleIsPaycheck: checked})} style={{flex: 1, alignSelf: 'center', display: 'block'}}/>
					</div>
					{!this.state.newRuleIsPaycheck && (
						<DropDownMenu value={this.state.newRuleFolderId} onChange={(e, key, value) => this.setState({newRuleFolderId: value})} iconStyle={{fill: 'black'}}>
							<MenuItem
								value="new"
								primaryText="Select a folder"
							/>
							{map(account.folders, folder => {
								if(!folder.permissions.move) return null;
								return (
									<MenuItem
										value={folder._id}
										primaryText={folder.name}
										rightIcon={<div className="material-icons">{folder.icon}</div>}
									/>
								);
							})}
						</DropDownMenu>
					)}
					{this.state.newRuleIsPaycheck && (
						<DropDownMenu value={this.state.newRuleBudgetProfileId} onChange={(e, key, value) => this.setState({newRuleBudgetProfileId: value})} iconStyle={{fill: 'black'}}>
							<MenuItem
								value="new"
								primaryText="Select a Budget Profile"
							/>
							{map(account.budgetProfiles, budgetProfile => (
								<MenuItem
									value={budgetProfile._id}
									primaryText={budgetProfile.name}
								/>
							))}
						</DropDownMenu>
					)}
				<hr/>
				<RaisedButton label="Starts with" primary={true} onClick={() => this.newRuleParam('starts')}/>
				<RaisedButton label="Contains" primary={true} onClick={() => this.newRuleParam('contains')}/>
				<RaisedButton label="Ends with" primary={true} onClick={() => this.newRuleParam('ends')} />
				<RaisedButton label="Amount is" primary={true} onClick={() => this.newRuleParam('amount')} />
				<RaisedButton label="Amount is at least" primary={true} onClick={() => this.newRuleParam('amountMin')} />
				<RaisedButton label="Amount is at most" primary={true} onClick={() => this.newRuleParam('amountMax')}/>
				<RaisedButton label="Reset" secondary={true} onClick={() => this.setState({newRuleParams: []})}/>
				{map(this.state.newRuleParams, (param, index) => {
					let label = '';
					switch(param.field) {
						case 'starts':
							label = 'Starts with';
							break;
						case 'contains':
							label = 'Contains';
							break;
						case 'ends':
							label = 'Ends with';
							break;
						case 'amount':
							label = 'Amount is';
							break;
						case 'amountMin':
							label = 'Amount is at least';
							break;
						case 'amountMax':
							label = 'Amount is at most';
							break;
					}

					return (
						<div>
							<TextField floatingLabelText={label} value={param.val} onChange={(e, value) => {
								let newRuleParams = this.state.newRuleParams;
								newRuleParams[index].val = param.field.startsWith('amount') ? Number(value) : value;
								this.setState({newRuleParams});
							}} />
						</div>
					);
				})}
			</div>
		);

		if (this.state.selectedRuleId !== "new") {
			let rule = account.rules[this.state.selectedRuleId];
			dialogBody = (
				<div>
					<div style={{display: 'flex'}}>
						<div style={{flex: 1}}>
							<IconButton tooltip="Delete" onClick={() => this.removeRule()}>
								<ContentRemove />
							</IconButton>
							<TextField floatingLabelText="Rule Name" value={rule.name} onChange={(e, value) => this.editRuleName(value)}/>
						</div>
						<Checkbox label='Is Paycheck' checked={rule.isPaycheck} onCheck={(e, checked) => this.editRuleIsPaycheck(checked)} style={{flex: 1, alignSelf: 'center', display: 'block'}}/>
					</div>
					{!rule.isPaycheck && (
						<DropDownMenu value={rule.folderId} onChange={(e, key, value) => this.editRuleFolderId(value)} iconStyle={{fill: 'black'}}>
							{map(account.folders, folder => {
								if(!folder.permissions.move) return null;
								return (
									<MenuItem
										value={folder._id}
										primaryText={folder.name}
										rightIcon={<div className="material-icons">{folder.icon}</div>}
									/>
								);
							})}
						</DropDownMenu>
					)}
					{rule.isPaycheck && (
						<DropDownMenu value={rule.budgetProfileId} onChange={(e, key, value) => this.editRuleBudgetProfileId(value)} iconStyle={{fill: 'black'}}>
							{map(account.budgetProfiles, budgetProfile => (
								<MenuItem
									value={budgetProfile._id}
									primaryText={budgetProfile.name}
								/>
							))}
						</DropDownMenu>
					)}
					<hr/>
					<RaisedButton label="Starts with" primary={true} onClick={() => this.editRuleAddParam('starts')}/>
					<RaisedButton label="Contains" primary={true} onClick={() => this.editRuleAddParam('contains')}/>
					<RaisedButton label="Ends with" primary={true} onClick={() => this.editRuleAddParam('ends')}/>
					<RaisedButton label="Amount is" primary={true} onClick={() => this.editRuleAddParam('amount')} />
					<RaisedButton label="Amount is at least" primary={true} onClick={() => this.editRuleAddParam('amountMin')} />
					<RaisedButton label="Amount is at most" primary={true} onClick={() => this.editRuleAddParam('amountMax')} />
					<RaisedButton label="Reset" secondary={true} onClick={() => this.editRuleResetParams()}/>
					{map(rule.parameters, (param, index) => {
						let label = '';
						switch(param.field) {
							case 'starts':
								label = 'Starts with';
								break;
							case 'contains':
								label = 'Contains';
								break;
							case 'ends':
								label = 'Ends with';
								break;
							case 'amount':
								label = 'Amount is';
								break;
							case 'amountMin':
								label = 'Amount is at least';
								break;
							case 'amountMax':
								label = 'Amount is at most';
								break;
						}

						return (
							<div>
								<TextField floatingLabelText={label} value={param.val} onChange={(e, value) => this.editRuleParam(index, param.field.startsWith('amount') ? Number(value) : value)} />
							</div>
						);
					})}
				</div>
			);
		}

        return (
			<Dialog
				title="Change Rules"
				actions={[
					<FlatButton
						label="Done"
						primary={true}
						onClick={() => {
							toggleRulesModal(false);
							this.setState({
								selectedRuleId: 'new',
								newRuleName: '',
								newRuleFolderId: 'new',
								newRuleParams: [],
								newRuleIsPaycheck: false,
								newRuleBudgetProfileId: 'new',
							});
						}}
					/>,
				]}
				modal={true}
				open={rulesModalOpen}
				autoScrollBodyContent={true}
			>
				<DropDownMenu value={this.state.selectedRuleId} onChange={(e, key, value) => this.setState({selectedRuleId: value})} iconStyle={{fill: 'black'}}>
					<MenuItem
						value="new"
						primaryText="New"
					/>
					{map(sortBy(account.rules, 'name'), rule => (
						<MenuItem
							value={rule._id}
							primaryText={rule.name}
							secondaryText={get(account.folders[rule.folderId], 'name')}
						/>
					))}
				</DropDownMenu>
				{dialogBody}
			</Dialog>
        );
	}

	createRule() {
		this.props.createRuleClicked({
			name: this.state.newRuleName,
			folderId: this.state.newRuleFolderId,
			parameters: this.state.newRuleParams,
			isPaycheck: this.state.newRuleIsPaycheck,
			budgetProfileId: this.state.newRuleBudgetProfileId
		});
		this.setState({
			selectedRuleId: 'new',
			newRuleName: '',
			newRuleFolderId: 'new',
			newRuleParams: [],
			newRuleIsPaycheck: false,
			newRuleBudgetProfileId: 'new'
		});
	}

	newRuleParam(field) {
		let newRuleParams = this.state.newRuleParams;
		newRuleParams.push({
			field,
			not: false,
			val: ''
		});
		this.setState({newRuleParams});
	}

	editRuleName(name) {
		if(!name) {
			this.props.editedBadRuleName(this.state.selectedRuleId, name);
		} else {
			this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], name});
		}
	}

	editRuleFolderId(folderId) {
		this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], folderId});
	}

	editRuleAddParam(field) {
		let parameters = this.props.account.rules[this.state.selectedRuleId].parameters;
		parameters.push({
			field,
			not: false,
			val: ''
		});
		this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], parameters});
	}

	editRuleParam(index, val) {
		let parameters = this.props.account.rules[this.state.selectedRuleId].parameters;
		parameters[index].val = val;
		this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], parameters});
	}

	editRuleResetParams() {
		this.props.editedBadRuleParams(this.state.selectedRuleId, []);
	}

	editRuleIsPaycheck(isPaycheck) {
		let budgetProfileId = 'new';
		let folderId = 'new';
		if(isPaycheck) {
			const firstBudgetProfile = first(values(this.props.account.budgetProfiles));
			// ! This will act strange if no budget profiles are available
			budgetProfileId = firstBudgetProfile ? firstBudgetProfile._id : 'new'
		} else {
			const firstFolder = first(values(filter(this.props.account.folders, folder => folder.permissions.move)));
			// ! This will act strange if no budget profiles are available
			folderId = firstFolder ? firstFolder._id : 'new'
		}
		this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], isPaycheck, budgetProfileId, folderId});
	}

	editRuleBudgetProfileId(budgetProfileId) {
		this.props.editedRule({...this.props.account.rules[this.state.selectedRuleId], budgetProfileId});
	}

	removeRule() {
		this.props.removeRuleClicked(this.state.selectedRuleId);
		this.setState({selectedRuleId: 'new'});
	}
};
