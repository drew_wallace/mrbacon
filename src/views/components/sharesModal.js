import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, isEmpty, find, get, filter } from 'lodash';
import { Dialog, FlatButton, IconButton } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class SharesModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			newShareEmail: ''
		}
    }

    render() {
		let { account, sharesModalOpen, toggleSharesModal, removeShareClicked } = this.props;

        return (
			<Dialog
				title="Change Shares"
				actions={[
					<FlatButton
						label="Done"
						primary={true}
						onClick={() => {
							toggleSharesModal(false);
						}}
					/>,
				]}
				modal={true}
				open={sharesModalOpen}
				autoScrollBodyContent={true}
			>
			<IconButton tooltip="Create" onClick={() => this.createShare()} disabled={find(account.shares, share => share.email == this.state.newShareEmail) || !this.state.newShareEmail || !this.validEmail(this.state.newShareEmail) || this.state.newShareEmail == get(find(account.user.emails, email => email.type == 'account'), 'value')}>
				<ContentAdd />
			</IconButton>
			<TextField floatingLabelText="Email" value={this.state.newShareEmail} onChange={(e, value) => this.setState({newShareEmail: value})} autoFocus/>
			{map(filter(account.shares, share => share.userId == account.user.id), (share, _id) => {
				return (
					<div key={_id}>
						<IconButton tooltip="Delete" onClick={() => removeShareClicked(_id)}>
							<ContentRemove />
						</IconButton>
						<TextField floatingLabelText="Email" value={share.email} onChange={(e, value) => this.editShareEmail(_id, value)}/>
					</div>
				);
			})}
			</Dialog>
        );
	}

	createShare() {
		this.props.createShareClicked({
			email: this.state.newShareEmail
		});
		this.setState({
			newShareEmail: ''
		});
	}

	editShareEmail(shareId, email) {
		if(!email || !this.validEmail(email) || email == get(find(account.user.emails, email => email.type == 'account'), 'value')) {
			this.props.editedBadShareEmail(shareId, email);
		} else {
			this.props.editedShare({...this.props.account.shares[shareId], email});
		}
	}

	validEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
};
