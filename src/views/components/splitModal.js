import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, first, values, pickBy, get, set, keys } from 'lodash';
import { Dialog, FlatButton, RaisedButton , IconButton, DropDownMenu, MenuItem, Checkbox, Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class SplitModal extends Component {
    constructor(props) {
		super(props);

		this.state = {
			divisions: {}
		}
    }

    render() {
		let { account, splitModalOpen, toggleSplitModal, splitTransaction, selectedTransactions, changedSelectedTransactions, changedSelectedAllTransactions } = this.props;

        return (
			<Dialog
				title="Split Transaction"
				actions={[
					<FlatButton
						label="Close"
						primary={true}
						onClick={() => {
							toggleSplitModal(false);
							{/* changedSelectedTransactions([]);
							changedSelectedAllTransactions(0); */}
							this.setState({divisions: {}});
						}}
					/>,
					<FlatButton
						label="Submit"
						primary={true}
						disabled={keys(this.state.divisions).length == 0}
						onClick={() => {
							splitTransaction(first(values(selectedTransactions))._id, this.state.divisions);
							toggleSplitModal(false);
							changedSelectedTransactions([]);
							changedSelectedAllTransactions(0);
							this.setState({divisions: {}});
						}}
					/>,
				]}
				modal={true}
				open={splitModalOpen}
				autoScrollBodyContent={true}
			>
				<Table>
					<TableHeader adjustForCheckbox={false} displaySelectAll={false}>
						<TableRow>
							<TableHeaderColumn>Enabled</TableHeaderColumn>
							<TableHeaderColumn>Folder</TableHeaderColumn>
							<TableHeaderColumn>Amount</TableHeaderColumn>
							<TableHeaderColumn>Use Remaining</TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={false}>
						{map(account.data.folders, folder => {
							if(!folder.permissions.budgetProfile) return null;
							return (
								<TableRow key={folder._id} selectable={false}>
									<TableRowColumn>
										<Checkbox disabled={true} checked={has(this.state.divisions, folder._id)} onCheck={(e, isChecked) => this.toggleDivisions(folder._id, isChecked)}/>
									</TableRowColumn>
									<TableRowColumn>
										{/* TODO: include folder icon */}
										{folder.name}
									</TableRowColumn>
									<TableRowColumn>
										<TextField
											id={folder._id}
											value={get(this.state.divisions[folder._id], 'amount') == 'remaining' ? '' : get(this.state.divisions[folder._id], 'amount') || ''}
											onChange={(e, value) => this.divisionsAmount(folder._id, value)}
										/>
									</TableRowColumn>
									<TableRowColumn>
										<Checkbox checked={get(this.state.divisions[folder._id], 'amount') == 'remaining'} onCheck={(e, isChecked) => this.divisionsAmount(folder._id, isChecked ? 'remaining' : '')}/>
									</TableRowColumn>
								</TableRow>
							)
						})}
					</TableBody>
				</Table>
			</Dialog>
        );
	}

	toggleDivisions(folderId, isEnabled) {
		let divisions = this.state.divisions;

		if(isEnabled) {
			divisions[folderId] = '';
		} else {
			delete divisions[folderId];
		}

		this.setState({divisions});
	}

	divisionsAmount(folderId, amount) {
		let divisions = this.state.divisions;

		if(amount) {
			if(amount == 'remaining') {
				divisions = pickBy(divisions, division => division.amount != 'remaining');
				set(divisions, `${folderId}.amount`, amount);
			} else {
				set(divisions, `${folderId}.amount`, Number(amount));
			}
		} else {
			delete divisions[folderId];
		}

		this.setState({divisions});
	}
}