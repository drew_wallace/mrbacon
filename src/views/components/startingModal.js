import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, mapValues, keys, pickBy } from 'lodash';
import numeral from 'numeral';
import { Dialog, FlatButton, IconButton, DropDownMenu, MenuItem } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import PlayCircleOutline from 'material-ui/svg-icons/av/play-circle-outline';
import Sync from 'material-ui/svg-icons/notification/sync';

export default class StartingModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
		let { startingModalOpen, toggleStartingModal } = this.props;

        return (
			<Dialog
				title="Get Started"
				actions={[
					<FlatButton
						label="Done"
						primary={true}
						onClick={() => {
							toggleStartingModal(false);
						}}
					/>,
				]}
				modal={true}
				open={startingModalOpen}
			>
				Mr. Bacon needs a starting point. Click Sync <Sync /> to bring in your transactions. Select a transaction then click Start Budgeting <PlayCircleOutline />.
			</Dialog>
        );
	}
};
