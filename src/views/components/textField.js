import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { TextField as MaterialUiTextField } from 'material-ui';
// import 'whatwg-fetch';

export default class TextField extends Component {
	constructor(props) {
		super(props);

		this.handleChange = this.handleChange.bind(this);
		this._rawStr = '';
		this._caretPosition = 0;
	}


	componentDidUpdate({ value }) {
		if(this.props.value !== value) {
			const str = this._rawStr.substr(0, this._caretPosition);
			const index = String(this.props.value).indexOf(str) + this._caretPosition;

			if(index !== -1) {
				this.refs.textfield.input.selectionStart = this.refs.textfield.input.selectionEnd = index;
			}
		}
	}

	handleChange(ev) {
		this._rawStr = String(ev.target.value);
		this._caretPosition = Number(ev.target.selectionEnd);

		if(this.props.onChange) {
			this.props.onChange(ev, this._rawStr);
		}
	}

	render() {
		return (<MaterialUiTextField {...this.props} ref="textfield" onChange={this.handleChange} />);
	}
};
