import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { map, has, mapValues, keys, pickBy } from 'lodash';
import numeral from 'numeral';
import { Dialog, FlatButton, IconButton, DropDownMenu, MenuItem } from 'material-ui';
import TextField from './textField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

export default class TransferModal extends Component {
    constructor(props) {
        super(props);

		this.state = {
			selectedFromFolderId: '',
			selectedToFolderId: '',
			amount: ''
		}
    }

    render() {
		let { account, transferModalOpen, toggleTransferModal, transferedAmount } = this.props;

        return (
			<Dialog
				title="Transfer Amount"
				actions={[
					<FlatButton
						label="Close"
						primary={true}
						onClick={() => {
							toggleTransferModal(false);
							this.setState({selectedFromFolderId: '', selectedToFolderId: '', amount: ''});
						}}
					/>,
					<FlatButton
						label="Submit"
						primary={true}
						disabled={!this.state.selectedFromFolderId || !this.state.selectedToFolderId || !this.state.amount}
						onClick={() => {
							transferedAmount(this.state.selectedFromFolderId, this.state.selectedToFolderId, Number(this.state.amount));
							toggleTransferModal(false);
							this.setState({selectedFromFolderId: '', selectedToFolderId: '', amount: ''});
						}}
					/>,
				]}
				modal={true}
				open={transferModalOpen}
			>
				<div>
					<DropDownMenu value={this.state.selectedFromFolderId} onChange={(e, key, value) => this.setState({selectedFromFolderId: value})} iconStyle={{fill: 'black'}} listStyle={{minWidth: 250}}>
						<MenuItem
							value=""
							primaryText="From folder"
						/>
						{map(account.data.folders, folder => (
							folder.permissions.move && <MenuItem
								value={folder._id}
								primaryText={folder.name}
								secondaryText={numeral(folder.amount).format('$0,0.00')}
								label={`${folder.name} ${numeral(folder.amount).format('$0,0.00')}`}
								rightIcon={<div className="material-icons">{folder.icon}</div>}
							/>
						))}
					</DropDownMenu>
				</div>
				<div>
					<DropDownMenu value={this.state.selectedToFolderId} onChange={(e, key, value) => this.setState({selectedToFolderId: value})} iconStyle={{fill: 'black'}} listStyle={{minWidth: 250}}>
						<MenuItem
							value=""
							primaryText="To folder"
						/>
						{map(account.data.folders, folder => (
							folder.permissions.move && <MenuItem
								value={folder._id}
								primaryText={folder.name}
								secondaryText={numeral(folder.amount).format('$0,0.00')}
								label={`${folder.name} ${numeral(folder.amount).format('$0,0.00')}`}
								rightIcon={<div className="material-icons">{folder.icon}</div>}
							/>
						))}
					</DropDownMenu>
				</div>
				<div>
					<TextField value={this.state.amount} floatingLabelText='Amount' onChange={(e, amount) => this.setState({amount})}/>
				</div>
			</Dialog>
        );
	}
};
