import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import muiThemeable from 'material-ui/styles/muiThemeable';

import {fetchAccountIfNeeded, changedSelectedFolder, syncDataClicked, toggleFoldersModal, toggleRulesModal, toggleBudgetProfilesModal, createBudgetClicked, removedTransactions, toggleSyncModal, toggleMoveModal, changedSelectedTransactions, changedSelectedAllTransactions, changedSelectedBudget, exportData, fixData, toggleImportDataModal, toggleSharesModal, changedSelectedUser, changedSelectedBudgetId, hideTransactions, toggleSplitModal, toggleMergeModal, toggleTransferModal, toggleStartingModal, toggleMoveBudgetModal } from '../../actions';

import Account from '../components/account';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        selectedFolderId: state.selectedFolderId,
        selectedTransactions: state.selectedTransactions,
        selectedAllTransactions: state.selectedAllTransactions,
        selectedBudgetId: state.selectedBudgetId,
        selectedUserId: state.selectedUserId
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAccountIfNeeded: () => dispatch(fetchAccountIfNeeded()),
        changedSelectedFolder: (id) => dispatch(changedSelectedFolder(id)),
        toggleFoldersModal: (open) => dispatch(toggleFoldersModal(open)),
        toggleRulesModal: (open) => dispatch(toggleRulesModal(open)),
        toggleBudgetProfilesModal: (open) => dispatch(toggleBudgetProfilesModal(open)),
        createBudgetClicked: () => dispatch(createBudgetClicked()),
        removedTransactions: (transactionIds) => dispatch(removedTransactions(transactionIds)),
        toggleSyncModal: (open) => dispatch(toggleSyncModal(open)),
        toggleMoveModal: (open) => dispatch(toggleMoveModal(open)),
        toggleMoveBudgetModal: (open) => dispatch(toggleMoveBudgetModal(open)),
        changedSelectedTransactions: (transactions) => dispatch(changedSelectedTransactions(transactions)),
        changedSelectedAllTransactions: (selectAll) => dispatch(changedSelectedAllTransactions(selectAll)),
        changedSelectedBudget: (id) => dispatch(changedSelectedBudget(id)),
        exportData: (id) => dispatch(exportData(id)),
        fixData: () => dispatch(fixData()),
        toggleImportDataModal: (open) => dispatch(toggleImportDataModal(open)),
        toggleSharesModal: (open) => dispatch(toggleSharesModal(open)),
        changedSelectedUser: (id) => dispatch(changedSelectedUser(id)),
        changedSelectedBudgetId: (id) => dispatch(changedSelectedBudgetId(id)),
        hideTransactions: (transactionIds, hide) => dispatch(hideTransactions(transactionIds, hide)),
        toggleSplitModal: (open) => dispatch(toggleSplitModal(open)),
        toggleMergeModal: (open) => dispatch(toggleMergeModal(open)),
        toggleTransferModal: (open) => dispatch(toggleTransferModal(open)),
        toggleStartingModal: (open) => dispatch(toggleStartingModal(open))
    };
}

const AccountLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Account);

export default muiThemeable()(AccountLayout);