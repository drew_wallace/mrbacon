import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleBudgetProfilesModal, createBudgetProfileClicked, removeBudgetProfileClicked, editedBudgetProfile, editedBadBudgetProfileName } from '../../actions';

import BudgetProfiles from '../components/budgetProfilesModal';

const mapStateToProps = (state) => {
    return {
        budgetProfilesModalOpen: state.budgetProfilesModalOpen,
        account: state.account.data
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleBudgetProfilesModal: (open) => dispatch(toggleBudgetProfilesModal(open)),
        createBudgetProfileClicked: (budgetProfileName) => dispatch(createBudgetProfileClicked(budgetProfileName)),
        editedBudgetProfile: (budgetProfile) => dispatch(editedBudgetProfile(budgetProfile)),
        editedBadBudgetProfileName: (budgetProfileId, budgetProfileName) => dispatch(editedBadBudgetProfileName(budgetProfileId, budgetProfileName)),
        removeBudgetProfileClicked: (budgetProfileName) => dispatch(removeBudgetProfileClicked(budgetProfileName))
    };
}

const BudgetProfilesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(BudgetProfiles);

export default BudgetProfilesContainer;