import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleFoldersModal, createFolderClicked, removeFolderClicked, editedFolder, editedBadFolderName, editedBadFolderAmount } from '../../actions';

import FoldersModal from '../components/foldersModal';

const mapStateToProps = (state) => {
    return {
        foldersModalOpen: state.foldersModalOpen,
        account: state.account.data
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleFoldersModal: (open) => dispatch(toggleFoldersModal(open)),
        createFolderClicked: (folder) => dispatch(createFolderClicked(folder)),
        editedFolder: (folder) => dispatch(editedFolder(folder)),
        editedBadFolderName: (folderId, folderName) => dispatch(editedBadFolderName(folderId, folderName)),
        removeFolderClicked: (folderName) => dispatch(removeFolderClicked(folderName)),
        editedBadFolderAmount: (folderId, folderAmount) => dispatch(editedBadFolderAmount(folderId, folderAmount)),
    };
}

const FoldersModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(FoldersModal);

export default FoldersModalContainer;