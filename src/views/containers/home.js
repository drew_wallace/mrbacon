import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import Home from '../components/home';

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn
    };
}

const mapDispatchToProps = (dispatch) => {
    return {};
}

const HomeLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

export default HomeLayout;