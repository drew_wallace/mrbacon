import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleImportDataModal, importDataClicked } from '../../actions';

import ImportDataModal from '../components/importDataModal';

const mapStateToProps = (state) => {
    return {
        importDataModalOpen: state.importDataModalOpen
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        importDataClicked: (exportedData) => dispatch(importDataClicked(exportedData)),
        toggleImportDataModal: (open) => dispatch(toggleImportDataModal(open)),
    };
}

const ImportDataModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ImportDataModal);

export default ImportDataModalContainer;