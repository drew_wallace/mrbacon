import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { fetchBanksIfNeeded } from '../../actions';

import LinkAccounts from '../components/linkAccounts';

const mapStateToProps = (state) => {
    return {
        banks: state.banks
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchBanksIfNeeded: () => dispatch(fetchBanksIfNeeded())
    };
}

const LinkAccountsLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(LinkAccounts);

export default LinkAccountsLayout;