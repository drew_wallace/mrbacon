import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleMergeModal, mergedTransactions, changedSelectedTransactions, changedSelectedAllTransactions } from '../../actions';

import MergeModal from '../components/mergeModal';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        mergeModalOpen: state.mergeModalOpen,
        selectedTransactions: state.selectedTransactions
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        mergedTransactions: (transactionIds, folderId) => dispatch(mergedTransactions(transactionIds, folderId)),
        toggleMergeModal: (open) => dispatch(toggleMergeModal(open)),
        changedSelectedTransactions: (transactions) => dispatch(changedSelectedTransactions(transactions)),
        changedSelectedAllTransactions: (selectAll) => dispatch(changedSelectedAllTransactions(selectAll))
    };
}

const MergeModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MergeModal);

export default MergeModalContainer;