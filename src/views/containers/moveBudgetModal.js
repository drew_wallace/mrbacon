import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleMoveBudgetModal, moveBudgetTransactions, changedSelectedTransactions, changedSelectedAllTransactions } from '../../actions';

import MoveBudgetModal from '../components/moveBudgetModal';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        moveBudgetModalOpen: state.moveBudgetModalOpen,
        selectedTransactions: state.selectedTransactions
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        moveBudgetTransactions: (transactionIds, budgetId) => dispatch(moveBudgetTransactions(transactionIds, budgetId)),
        toggleMoveBudgetModal: (open) => dispatch(toggleMoveBudgetModal(open)),
        changedSelectedTransactions: (transactions) => dispatch(changedSelectedTransactions(transactions)),
        changedSelectedAllTransactions: (selectAll) => dispatch(changedSelectedAllTransactions(selectAll))
    };
}

const MoveBudgetModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MoveBudgetModal);

export default MoveBudgetModalContainer;