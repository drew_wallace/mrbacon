import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleMoveModal, movedTransactions, changedSelectedTransactions, changedSelectedAllTransactions } from '../../actions';

import MoveModal from '../components/moveModal';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        moveModalOpen: state.moveModalOpen,
        selectedTransactions: state.selectedTransactions
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        movedTransactions: (transactionIds, folderId) => dispatch(movedTransactions(transactionIds, folderId)),
        toggleMoveModal: (open) => dispatch(toggleMoveModal(open)),
        changedSelectedTransactions: (transactions) => dispatch(changedSelectedTransactions(transactions)),
        changedSelectedAllTransactions: (selectAll) => dispatch(changedSelectedAllTransactions(selectAll))
    };
}

const MoveModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MoveModal);

export default MoveModalContainer;