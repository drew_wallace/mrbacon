import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleRulesModal, createRuleClicked, removeRuleClicked, editedRule, editedBadRuleName, editedBadRuleParams } from '../../actions';

import RulesModal from '../components/rulesModal';

const mapStateToProps = (state) => {
    return {
        rulesModalOpen: state.rulesModalOpen,
        account: state.account.data
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleRulesModal: (open) => dispatch(toggleRulesModal(open)),
        createRuleClicked: (ruleName) => dispatch(createRuleClicked(ruleName)),
        editedRule: (rule) => dispatch(editedRule(rule)),
        editedBadRuleName: (ruleId, ruleName) => dispatch(editedBadRuleName(ruleId, ruleName)),
        removeRuleClicked: (ruleName) => dispatch(removeRuleClicked(ruleName)),
        editedBadRuleParams: (ruleId, ruleParams) => dispatch(editedBadRuleParams(ruleId, ruleParams))
    };
}

const RulesModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RulesModal);

export default RulesModalContainer;