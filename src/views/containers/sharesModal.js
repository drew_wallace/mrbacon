import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleSharesModal, createShareClicked, removeShareClicked, editedShare, editedBadShareEmail, editedBadShareAmount } from '../../actions';

import SharesModal from '../components/sharesModal';

const mapStateToProps = (state) => {
    return {
        sharesModalOpen: state.sharesModalOpen,
        account: state.account.data
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSharesModal: (open) => dispatch(toggleSharesModal(open)),
        createShareClicked: (folder) => dispatch(createShareClicked(folder)),
        editedShare: (folder) => dispatch(editedShare(folder)),
        editedBadShareEmail: (folderId, folderName) => dispatch(editedBadShareEmail(folderId, folderName)),
        removeShareClicked: (folderName) => dispatch(removeShareClicked(folderName))
    };
}

const SharesModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SharesModal);

export default SharesModalContainer;