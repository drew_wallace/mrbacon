import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleSplitModal, splitTransaction, changedSelectedTransactions, changedSelectedAllTransactions } from '../../actions';

import SplitModal from '../components/splitModal';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        splitModalOpen: state.splitModalOpen,
        selectedTransactions: state.selectedTransactions
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        splitTransaction: (transactionIds, folderId) => dispatch(splitTransaction(transactionIds, folderId)),
        toggleSplitModal: (open) => dispatch(toggleSplitModal(open)),
        changedSelectedTransactions: (transactions) => dispatch(changedSelectedTransactions(transactions)),
        changedSelectedAllTransactions: (selectAll) => dispatch(changedSelectedAllTransactions(selectAll))
    };
}

const SplitModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SplitModal);

export default SplitModalContainer;