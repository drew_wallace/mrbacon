import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleStartingModal } from '../../actions';

import StartingModal from '../components/startingModal';

const mapStateToProps = (state) => {
    return {
        startingModalOpen: state.startingModalOpen
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleStartingModal: (open) => dispatch(toggleStartingModal(open))
    };
}

const StartingModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(StartingModal);

export default StartingModalContainer;