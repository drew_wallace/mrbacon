import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleSyncModal, syncDataClicked } from '../../actions';

import SyncModal from '../components/syncModal';

const mapStateToProps = (state) => {
    return {
        syncModalOpen: state.syncModalOpen
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        syncDataClicked: (encCredentials) => dispatch(syncDataClicked(encCredentials)),
        toggleSyncModal: (open) => dispatch(toggleSyncModal(open)),
    };
}

const SyncModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SyncModal);

export default SyncModalContainer;