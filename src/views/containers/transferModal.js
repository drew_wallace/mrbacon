import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import { toggleTransferModal, transferedAmount } from '../../actions';

import TransferModal from '../components/transferModal';

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transferModalOpen: state.transferModalOpen
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        transferedAmount: (transactionIds, folderId, amount) => dispatch(transferedAmount(transactionIds, folderId, amount)),
        toggleTransferModal: (open) => dispatch(toggleTransferModal(open))
    };
}

const TransferModalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TransferModal);

export default TransferModalContainer;