module.exports = [
    {
        name: 'Mr. Bacon',
        formId: 'mrbacon',
        thumbnail: 'https://s3.amazonaws.com/duxsite-thesandbox2/05c03788b014415ad92afa1a6482c5fca3e368ad8b43121e2a7a79eb47e34441u596.png'
    },
    {
        name: "Wells Fargo",
        thumbnail: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wells_Fargo_Bank.svg/1024px-Wells_Fargo_Bank.svg.png",
        route: "/api/link/wellsfargo",
        formId: "wellsfargo",
        form: [
            {
                name: 'username',
                type: 'text',
                required: true,
                autoFocus: true,
                placeholder: 'Username'
            },
            {
                name: 'password',
                type: 'password',
                required: true,
                placeholder: 'Password'
            }
        ]
    },
    {
        name: "USAA",
        thumbnail: "https://lh6.googleusercontent.com/-J_qdZ9IaEPc/AAAAAAAAAAI/AAAAAAAAAPU/aHe7Qpt6obA/s0-c-k-no-ns/photo.jpg",
        route: "/api/link/usaa",
        formId: "usaa",
        form: [
            {
                name: 'user',
                type: 'text',
                required: true,
                autoFocus: true,
                placeholder: 'USAA Member ID'
            },
            {
                name: 'password',
                type: 'password',
                required: true,
                placeholder: 'PIN'
            },
            {
                name: 'accId',
                type: 'password',
                required: true,
                placeholder: 'Account Number'
            },
            {
                name: 'accType',
                type: 'select',
                required: true,
                defaultValue: 'CREDITCARD',
                options: [
                    // {
                    //     value: 'CHECKING',
                    //     label: 'Checking'
                    // },
                    // {
                    //     value: 'SAVINGS',
                    //     label: 'Savings'
                    // },
                    // {
                    //     value: 'MONEYMRKT',
                    //     label: 'Money Market'
                    // },
                    {
                        value: 'CREDITCARD',
                        label: 'Credit Card'
                    }
                ]
            },
            // {
            //     name: 'bankId',
            //     type: 'text',
            //     placeholder: 'Routing Number (Checking/Savings)'
            // }
        ]
    }
];